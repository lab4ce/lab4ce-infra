#!/bin/bash
#Project: Lab4CE
#OS System Version: 20.04.2 LTS (Focal Fossa)
#OpenStack Version: openstack 5.4.0 Victoria 22th
#OpenStack Services Installed: Keystone, Glance, Placement, Nova, Neutron, Cinder, Kuryr, Zun

EXIT_SUCCESS=0
EXIT_FAILURE=1

DEFAULT_DELAY=1  #seconds
DEFAULT_NB_LINES=20

SEVERITY_TAB=("EMERGENCY" "ALERT" "CRITICAL" "ERROR" "WARNING" "WARN" "NOTINE" "INFO" "DEBUG")

green='\e[0;32m'
yellow='\e[0;33m'
grey='\e[0;37m'
redb='\e[0;31;1m'

function help(){
	echo -e """Usage: ${0##*/} [[--clear] [--severity SEVERITY] [--delay DELAY]]

  -h, --help\t Print this message
  -c, --clear\t Clear all log files into the directory
  -s, --severity Specify the severity
  -l, --lines\t Number of lines to display
  -d, --delay\t Specify a delay (seconds)

  e.g.  ./${0##*/} --clear [PATH]
	./${0##*/} --severity ERROR,WARNING,CRITICAL --lines 75 [PATH]
	./${0##*/} --severity none --delay 0.5 [PATH]"""
}

function error(){
	if [ $# -gt 2 ] ; then
		exit $EXIT_FAILURE
	fi

	echo -e "${redb}Erreur:${grey} $1"
}

if [ $# -eq 0 ] ; then
	help
	exit $EXIT_SUCCESS
fi

while [ $# -gt 0 ] ; do
	key="$1"

	case $key in
		-h | --help )
			help
			exit $EXIT_SUCCESS
			;;

		-c | --clear )
			CLEAR="y"
			shift
			;;

		-s | --severity )
			if [ -z $2 ] ; then
				POSITIONAL+=("$1")
			elif [ "$2" = "none" ] ; then
				SEVERITY="NONE"
			else
				SEVERITY="$2"
			fi
			shift
			shift
			;;

		-l | --lines )
			LINES="$2"
			shift
			shift
			;;

		-d | --delay )
			DELAY="$2"
			shift
			shift
			;;

		*)
			POSITIONAL+=("$1") # save it in an array for later
			shift
		;;
	esac
done

if [ ${#POSITIONAL[@]} -ne 1 ] ; then
	error "Un seul répertoire de logs doit être spécifié"
	exit $EXIT_FAILURE
fi

if [ -d ${POSITIONAL[0]} ] ; then
	REP=${POSITIONAL[0]}
elif [ -f ${POSITIONAL[0]} ] ; then
	if [ $(echo -e ${POSITIONAL[0]} | cut -d '.' -f 2) = "log" ] ; then
		FILE=${POSITIONAL[0]}
	else
		error "Le fichier spécifié n'est pas un fichier log"
			exit $EXIT_FAILURE
	fi
else
	error "L'emplacement spécifié est incorrect"
	exit $EXIT_FAILURE
fi

if [ ! -z $REP ] ; then
	for file in $(ls -1F $REP) ; do
		file=$(echo -e "$REP/$file" | sed 's/\/\//\//g')
		if [ $(echo -e $file | cut -d '.' -f 2) = "log" ] ; then
			FILE+=("$file")
		fi
	done
fi

if [ ${#FILE[@]} -eq 0 ] ; then
	error "Aucun fichier log n'existe dans le répertoire spécifié"
	exit $EXIT_FAILURE
fi

if [ "$CLEAR" = "y" ] ; then
	for file in ${FILE[@]} ; do
		echo > $file 1> /dev/null
	done
fi

if [ ! -z "$SEVERITY" ] ; then

	is_a_int_or_float='^[0-9]+([.][0-9]+)?$'
		if [[ $DELAY =~ $is_a_int_or_float ]] ; then
				DEFAULT_DELAY=$DELAY
		fi

	is_a_int='^[0-9]+$'
		if [[ $LINES =~ $is_a_int ]] ; then
		DEFAULT_NB_LINES=$LINES
	fi

	if [ "$SEVERITY" != "NONE" ] ; then
		for input_msg in $(echo -e "$SEVERITY" | tr ',' ' ') ; do
			check=0
			for tab_msg in ${SEVERITY_TAB[@]} ; do
				if [ "${input_msg^^}" = "${tab_msg^^}" ] ; then
					check=1
				fi
			done
			if [ $check -ne 1 ] ; then
				error "Un des niveau de sécurité est incorrect"
				echo -e "${SEVERITY_TAB[@]}" | sed 's/ /, /g'
				exit $EXIT_FAILURE
			fi
		done

		input_msg_format=$(echo -e "$SEVERITY" | tr ',' '|')
	fi

	while true ; do
		current_date=$(date +%T)
		clear
		echo -e "TIME ($current_date)\t $DEFAULT_DELAY(s)"

		for file in ${FILE[@]} ; do
			echo -e "\n$yellow$file$grey"
			if [ "$SEVERITY" = "NONE" ] ; then
				tail -n $DEFAULT_NB_LINES $file
			else
				tail -n $DEFAULT_NB_LINES $file | grep -E "$input_msg_format" --color
			fi
		done
		sleep $DEFAULT_DELAY
	done
	exit $EXIT_SUCCESS
fi
