#!/bin/bash
#Project: Lab4CE
#OS System Version: 20.04.2 LTS (Focal Fossa)
#OpenStack Version: openstack 5.4.0 Victoria 22th
#OpenStack Services Installed: Keystone, Glance, Placement, Nova, Neutron, Cinder, Kuryr, Zun

EXIT_SUCCESS=0
EXIT_FAILURE=1

green='\e[0;32m'
yellow='\e[0;33m'
grey='\e[0;37m'
redb='\e[0;31;1m'
white='\e[0;37;1m'

binaries=('ss' 'service' 'grep' 'wc')

services=('apache2' 'chrony' 'cinder-scheduler' 'cinder-volume' 'containerd' 'docker' 'etcd' 'glance-api' 
			'kuryr-libnetwork' 'memcached' 'mysql' 'neutron-dhcp-agent' 'neutron-l3-agent' 'neutron-metadata-agent' 
			'neutron-openvswitch-agent' 'neutron-server' 'nova-api' 'nova-compute' 'openvswitch-switch' 
			'systemd-networkd' 'tgt' 'zun-api' 'zun-cni-daemon' 'zun-compute' 'zun-wsproxy')

function error(){
	if [ $# -ne 1 ] ; then
		exit $EXIT_FAILURE
	fi

	ErrText="Erreur:"
	echo -e "$redb$ErrText$grey $1"
}

function check_service(){
	service_state=$(systemctl is-active $1)

	if [ "$service_state" != "active" ] ; then
		return 1	#Service not started
	fi
	return 0	#No problem
}

if [ $EUID -ne 0 ] ; then
	error "You not have root privileges"
	exit $EXIT_SUCCESS
fi

#Check exist binaries
for bin in ${binaries[@]} ; do
	if ! which $bin 1> /dev/null ; then
		error "Binaries $bin not exist"
		exit $EXIT_SUCCESS
	fi
done

listen_connection=$(sudo ss -puntl | grep -e "LISTEN")

c=0 #Compteur
for deamon in ${services[@]} ; do
	check_service $deamon

	if [ $? -eq 1 ] ; then
		c=$(($c + 1))
		echo -e "- $yellow${deamon}$grey not active"
	fi
done

if [ $c -eq 0 ] ; then
	echo -e "$grey${#services[@]}/${#services[@]}   $green All services started$grey"
else
	echo -e "[+] $yellow$c/${#services[@]}$grey service(s) not started"
	echo -e "\n[i] If you just reboot, please wait for 5 minutes the time the services starts then retry '$white${0##*/}$grey'"
fi

exit $EXIT_SUCCESS
