#!/bin/bash
#Project: Lab4CE
#OS System Version: 20.04.2 LTS (Focal Fossa)
#OpenStack Version: openstack 5.4.0 Victoria 22th
#OpenStack Services Installed: Keystone, Glance, Placement, Nova, Neutron, Cinder, Kuryr, Zun

EXIT_SUCCESS=0
EXIT_FAILURE=1

green='\e[0;32m'
yellow='\e[0;33m'
grey='\e[0;37m'
redb='\e[0;31;1m'

function help(){
	echo -e """Usage: ${0##*/} [PATH]

  -h, --help\t Print this message

  e.g.  ./${0##*/} /etc/nova/nova.conf
"""
}

function error(){
	if [ $# -ne 1 ] ; then
		exit $EXIT_FAILURE
	fi

	ErrText="Erreur:"
	echo -e "$redb$ErrText$grey $1"
}

if [ $# -ne 1 ] ; then
	error "Le nombre d'argument est incorrect"
	help
	exit $EXIT_SUCCESS
fi

if [ "$1" = "-h" -o "$1" = "--help" ] ; then
	help
	exit $EXIT_SUCCESS
fi

if [ ! -f $1 ] ; then
	error "Le fichier en paramètre n'existe pas"
	help
	exit $EXIT_SUCCESS
fi

for i in $(cat $1 | grep -v "#" | grep -E "=|\[" | tr ' ' 'µ') ; do
	d=$(echo -e "$i" | tr 'µ' ' ')

	if echo -e "$d" | grep -e "\[" > /dev/null ; then
		echo -e "$green$d$grey"
	else
		d1=$(echo -e "$d" | cut -d '=' -f 1)
		d2=$(echo -e "$d" | cut -d '=' -f 2)
		echo -e "\t$grey$d1$grey=$yellow$d2$grey"
	fi
done
