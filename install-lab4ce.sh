#!/bin/bash
#coding:utf-8
#Licence: CeCILL-B
#Project: Lab4CE
#Author: Quentin Récolé

############ http://cecill.info/licences/Licence_CeCILL-B_V1-fr.txt ###########
#                                                                             #
#  CONTRAT DE LICENCE DE LOGICIEL LIBRE CeCILL-B                              #
#                                                                             #
#                                                                             #
#      Avertissement                                                          #
#                                                                             #
#  Ce contrat est une licence de logiciel libre issue d'une concertation      #
#  entre ses auteurs afin que le respect de deux grands principes préside à   #
#  sa rédaction:                                                              #
#                                                                             #
#      * d'une part, le respect des principes de diffusion des logiciels      #
#        libres: accés au code source, droits étendus conférés aux            #
#        utilisateurs,                                                        #
#      * d'autre part, la désignation d'un droit applicable, le droit         #
#        français, auquel elle est conforme, tant au regard du droit de la    #
#        responsabilité civile que du droit de la propriété intellectuelle    #
#        et de la protection qu'il offre aux auteurs et titulaires des        #
#        droits patrimoniaux sur un logiciel.                                 #
#                                                                             #
#  ....                                                                       #
###############################################################################

CURRENT_PATH=$(pwd)

EXIT_SUCCESS=0
EXIT_FAILURE=1

BINFILES=('curl' 'wget' 'git')
BINPACKAGES=('curl' 'wget' 'git')

OPENSTACK_VERSION_NUMBER="22th"
OPENSTACK_VERSION_NAME="Victoria"

PROJECT_NAME="Lab4CE"
PROJECT_VERSION="5.0"

VERSION_SCRIPT="0.1"

#colors
green='\e[0;32m'
yellow='\e[0;33m'
grey='\e[0;37m'
redb='\e[0;31;1m'
white='\e[0;37;1m'

trap ctrl_c INT

function check_restore_point(){
	if [ ! -z $check_point ] ; then
		echo -e "$check_point" | sed 's/install_//g' > $CURRENT_PATH/install.restore
	fi
}

function ctrl_c(){
	echo -e "\n"
	read -n 1 -p "[?] Confirm to exit (Y/n) " quit

	if [[ $quit =~ ^[YyOo]$ ]] ; then
		check_restore_point
		exit $EXIT_FAILURE
	fi
}

function help(){
	echo -e """Usage: ${0##*/} [[--install] [--restore CHECKPOINT]] [--cpu]

  -h, --help\t Print this message
  -i, --install\t Install OpenStack for $PROJECT_NAME
  -r, --restore\t Restore from a checkpoint 
  -v, --version\t Show current script version
  -c, --cpu\t Display the number of system CPU core available

  e.g.  ./${0##*/} --install --restore [CHECKPOINT]"""
}

function error(){
	if [ $# -gt 2 ] ; then
		exit $EXIT_FAILURE
	fi

	if [ "$LANG" = "fr_FR.UTF-8" ] ; then
		echo -e "\n${redb}Erreur:${grey} $1"
	else
		echo -e "\n${redb}Error:${grey} $2"
	fi
	check_restore_point
}

function banner(){
	echo -e """$grey
\t   __       _     _  _     ___   __ 
\t  / /  __ _| |__ | || |   / __\\ /__\\
\t / /  / _' | '_ \| || |_ / /   /_\  
\t/ /__| (_| | |_) |__   _/ /___//__  
\t\____/\__,_|_.__/   |_| \____/\__/ V.$PROJECT_VERSION

                                ${white}OpenStack $OPENSTACK_VERSION_NAME $OPENSTACK_VERSION_NUMBER${grey}"""
}

function recover_variables(){
	if [ -f $CURRENT_PATH/variables.txt ] ; then
		controller_ip=$(sed -rn 's/^CONTROLLER_IP=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\n\t${white}CONTROLLER_IP = $grey$controller_ip"
		controller_nic_name=$(sed -rn 's/^CONTROLLER_NIC_NAME=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}CONTROLLER_NIC_NAME = $grey$controller_nic_name"
		rabbit_pass=$(sed -rn 's/^RABBIT_PASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}RABBIT_PASS = $grey$rabbit_pass"
		metadata_secret=$(sed -rn 's/^METADATA_SECRET=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}METADATA_SECRET = $grey$metadata_secret"
		dash_dbpass=$(sed -rn 's/^DASH_DBPASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}DASH_DBPASS = $grey$dash_dbpass"
		mysql_superuser_dbpass=$(sed -rn 's/^MYSQL_SUPERUSER_DBPASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}MYSQL_SUPERUSER_DBPASS = $grey$mysql_superuser_dbpass"
		keystone_dbpass=$(sed -rn 's/^KEYSTONE_DBPASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}KEYSTONE_DBPASS = $grey$keystone_dbpass"
		glance_dbpass=$(sed -rn 's/^GLANCE_DBPASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}GLANCE_DBPASS = $grey$glance_dbpass"
		placement_dbpass=$(sed -rn 's/^PLACEMENT_DBPASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}PLACEMENT_DBPASS = $grey$placement_dbpass"
		nova_dbpass=$(sed -rn 's/^NOVA_DBPASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}NOVA_DBPASS = $grey$nova_dbpass"
		neutron_dbpass=$(sed -rn 's/^NEUTRON_DBPASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}NEUTRON_DBPASS = $grey$neutron_dbpass"
		cinder_dbpass=$(sed -rn 's/^CINDER_DBPASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}CINDER_DBPASS = $grey$cinder_dbpass"
		zun_dbpass=$(sed -rn 's/^ZUN_DBPASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}ZUN_DBPASS = $grey$zun_dbpass"
		admin_pass=$(sed -rn 's/^ADMIN_PASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}ADMIN_PASS = $grey$admin_pass"
		glance_pass=$(sed -rn 's/^GLANCE_PASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}GLANCE_PASS = $grey$glance_pass"
		placement_pass=$(sed -rn 's/^PLACEMENT_PASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}PLACEMENT_PASS = $grey$placement_pass"
		nova_pass=$(sed -rn 's/^NOVA_PASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}NOVA_PASS = $grey$nova_pass"
		neutron_pass=$(sed -rn 's/^NEUTRON_PASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}NEUTRON_PASS = $grey$neutron_pass"
		cinder_pass=$(sed -rn 's/^CINDER_PASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}CINDER_PASS = $grey$cinder_pass"
		kuryr_pass=$(sed -rn 's/^KURYR_PASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}KURYR_PASS = $grey$kuryr_pass"
		zun_pass=$(sed -rn 's/^ZUN_PASS=([^\n]+)$/\1/p' $CURRENT_PATH/variables.txt) && echo -e "\t${white}ZUN_PASS = $grey$zun_pass"
	else
		error "Le fichier 'variables.txt' est introuvable" "File 'variables.txt' does not exist"
		exit $EXIT_FAILURE
	fi
}

function service_restart(){
	while [ $# -ne 0 ] ; do
		echo -en "\rRestart '$1' service ..."
		service $1 restart 1> /dev/null 2>&1

		service_state=$(systemctl is-active $1)
		
		if [ "$service_state" = "active" ] ; then
			echo -e "\r$green[+]$grey Restart of '$1' service Success"
		else
			error "Le service '$1' n'a pas redémarré correctement" "Service '$1' has not restarted correctly"
			exit $EXIT_FAILURE
		fi

		shift
	done
}

function check_env_var_defined(){
	if [ -f ~/admin-openrc ] ; then
		. ~/admin-openrc 1> /dev/null

		if [ -z $OS_PROJECT_DOMAIN_NAME ] ; then
			error "Les variables d'environnement d'OpenStack ne sont pas définies" "OpenStack global environment variables not correctly defined"
			exit $EXIT_FAILURE
		fi
	else
		error "Le fichier '~/admin-openrc' est introuvable, les variables d'environnement d'OpenStack ne sont pas définies" "The '~/admin-openrc' file is not found, OpenStack global environment variables not correctly defined"
		exit $EXIT_FAILURE
	fi
}

function check_ip(){
    local  IPA1=$1
    local  stat=1

	if [[ $IPA1 =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]] ; then
		OIFS=$IFS

		IFS='.'
		ip=($ip)
		IFS=$OIFS

		[[ ${ip[0]} -le 255 && ${ip[1]} -le 255 && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
		stat=$?
    fi
    return $stat
}

function check_mysql_db_created(){
	while [ $# -ne 0 ] ; do
		if ! mysql -u root --password='$esc_mysql_superuser_dbpass' -e "SHOW DATABASES;" --batch | grep -e "$1" 1> /dev/null ; then
			error "La base de données '$1' n'a pas été créée" "The '$1' database has not been created"
			exit $EXIT_FAILURE
		fi

		shift
	done
}

function check_mysql_user_created(){
	while [ $# -ne 0 ] ; do
		if ! mysql -u root --password='$esc_mysql_superuser_dbpass' -e "SELECT host, user FROM mysql.user ORDER BY user;" --batch | grep -e "$1" 1> /dev/null ; then
			error "L'utilisateur MySQL '$1' n'a pas été créé" "The '$1' MySQL user has not been created"
			exit $EXIT_FAILURE
		fi

		shift
	done
}

function check_populate(){
	while [ $# -ne 0 ] ; do
		check_populate=$(mysql -u root --password='$esc_mysql_superuser_dbpass' -e "USE $1; SHOW TABLES;")
		if [ -z "$check_populate" ] ; then
			error "La table '$1' n'a pas été créée" "The '$1' table has not been created"
			exit $EXIT_FAILURE
		else
			echo -e "\r$green[+]$grey Populate the '$1' database Success"
		fi

		shift
	done
}

function apt_package(){
	while [ $# -ne 0 ] ; do
		echo -en "\rInstallation of '$1' in progress ..."
		apt install -y $1 1> /dev/null 2>&1
		echo -e "\r$green[+]$grey Installation of '$1' Success    "

		shift
	done
}

function pip3_package(){
	while [ $# -ne 0 ] ; do
		echo -en "\rInstallation of '$1' in progress ..."
		pip3 install $1 1> /dev/null 2>&1
		echo -e "\r$green[+]$grey Installation of '$1' Success    "

		shift
	done
}

function basic_single_escape(){
	echo "$1" | sed 's/\(['"'"'\]\)/\\\1/g'
}

source $CURRENT_PATH/openstack/lab4ce-keystone.sh
source $CURRENT_PATH/openstack/lab4ce-glance.sh
source $CURRENT_PATH/openstack/lab4ce-placement.sh
source $CURRENT_PATH/openstack/lab4ce-nova.sh
source $CURRENT_PATH/openstack/lab4ce-neutron.sh
source $CURRENT_PATH/openstack/lab4ce-horizon.sh
source $CURRENT_PATH/openstack/lab4ce-cinder.sh
source $CURRENT_PATH/openstack/lab4ce-kuryr.sh
source $CURRENT_PATH/openstack/lab4ce-zun.sh
source $CURRENT_PATH/openstack/lab4ce-others.sh

if [ $# -eq 0 ] ; then
	error "Aucun paramètre spécifié" "No parameter specified"
	help
	exit $EXIT_FAILURE
fi

while [ $# -gt 0 ] ; do
	key="$1"

	case $key in
		-h | --help )
			help
			exit $EXIT_FAILURE
			;;

		-v | --version )
			echo -e "version $VERSION_SCRIPT"
			exit $EXIT_FAILURE
			;;

		-i | --install )
			INSTALL=1
			shift
			;;

		-r | --restore )
			LAST_STEP_ARG=1
			LAST_STEP=$2
			shift
			shift
			;;

		-c | --cpu )
			CPU_CORE=1
			shift
			;;

		*)
			POSITIONAL+=("$1")  # save it in an array for later
			shift
			;;
	esac
done

if [ ${#POSITIONAL[@]} -ne 0 ] ; then
	error "Trop d'arguments" "Too many arguments"
	help
	exit $EXIT_FAILURE
fi

# Check Root privileges
if [ $EUID -ne 0 ] ; then
	error "Vous n'avez pas les privilèges root" "You not have root privileges"
	exit $EXIT_FAILURE
fi

if [ "$CPU_CORE" = "1" ] ; then
	if [ -f /proc/cpuinfo ] ; then
		cpus=0
		for i in $(cat /proc/cpuinfo | grep -e "cpu cores" | cut -d ':' -f 2 | sed 's/ //g'); do
			cpus=$(($cpus + $i))
		done
		echo -e "$cpus"
		exit $EXIT_SUCCESS
	else
		error "Le fichier '/proc/cpuinfo' n'existe pas" "'/proc/cpuinfo' file does not exist"
		exit $EXIT_FAILURE
	fi
fi

# Check Internet connection
if [ $(ping 8.8.8.8 -c 1 -W 0.5 | grep -e "received" | cut -d ' ' -f 4) -ne 1 ] ; then
	error "Aucun accès Internet" "No Internet access"
	exit $EXIT_FAILURE
fi

# Check DNS resolution
if [ $(ping www.openstack.org -c 1 -W 0.5 | grep -e "received" | cut -d ' ' -f 4) -ne 1 ] ; then
	error "Erreur dans la résolution DNS pour 'www.openstack.org'" "Error in DNS resolution for 'www.openstack.org'"
	exit $EXIT_FAILURE
fi

# Restore file
if [ "$INSTALL" = "1" ] ; then

	# Argument is empty (-r)
	if [[ ! -z $LAST_STEP_ARG && -z "$LAST_STEP" ]] ; then
		error "Le point de restauration est incorrect" "Restore point is incorrect"
		echo -e "List available restore point:"

		for i in $(declare -F | grep -e "install_" | cut -d ' ' -f 3) ; do
			alias_checkpoint=$(echo -e "$i" | sed 's/install_//g')
			echo -e " - $alias_checkpoint"
		done

		exit $EXIT_FAILURE
	fi

	# Argument is specify (-r [CHECKPOINT])
	if [[ ! -z $LAST_STEP_ARG && ! -z "$LAST_STEP" ]] ; then
		START_RESTORE="1"

	# Checkpoint from the restore file (install.restore)
	elif [ -f $CURRENT_PATH/install.restore ] ; then
		LAST_STEP=$(cat $CURRENT_PATH/install.restore)

		read -n 1 -p "[?] Resume installation (Y/n) " resume

		if [[ $resume =~ ^[YyOo]$ ]] ; then
			START_RESTORE="1"
		elif [[ $resume =~ ^[Nn]$ ]] ; then
			rm -f $CURRENT_PATH/install.restore 1> /dev/null 2>&1
			echo -e "\n$green[+]$grey 'install.restore' file deleted"
			exit $EXIT_FAILURE
		fi
	fi

	if [ "$START_RESTORE" = "1" ] ; then

		for i in $(declare -F | grep -e "install_" | cut -d ' ' -f 3) ; do
			alias_checkpoint=$(echo -e "$i" | sed 's/install_//g')
			function_name=$(echo -e "$i")

			list_alias+=("$alias_checkpoint")

			if [ "$LAST_STEP" = "$alias_checkpoint" ] ; then
				recover_variables

				# If the 1st variable 'CONTROLLER_IP' is empty (empty by default)
				if [ -z $controller_ip ] ; then
					error "La variable 'CONTROLLER_IP' n'est pas définie" "The 'CONTROLLER_IP' variable is not defined"
					exit $EXIT_FAILURE
				fi

				$function_name

				exit $EXIT_SUCCESS
			fi
		done

		error "Le point de restauration est incorrect" "Restore point is incorrect"
		echo -e "List available restore point:"
		for alias in ${list_alias[@]} ; do
			echo -e " - $alias"
		done

		exit $EXIT_FAILURE
	fi
fi

# Updates
read -n 1 -p "[?] Make system updates now (Y/n) " update

if [[ $update =~ ^[YyOo]$ ]] ; then
	echo -e "\n$white[*]$grey Update in progress..."
	sudo apt update && sudo apt -y dist-upgrade
	clear
	echo -e "$green[+]$grey Updates\t OK"
fi

# Check binaries installed
for i in $(seq 0 $((${#BINFILES[@]} - 1))) ; do
	if ! which ${BINFILES[$i]} 1> /dev/null ; then
		echo -e "$yellow[!]$grey '${BINFILES[$i]}' not installed"
		apt_package ${BINPACKAGES[$i]}
	fi
done

# OS Information
if [ -f /etc/os-release ] ; then
	os_release_content=$(cat /etc/os-release)
	OS_VERSION_ID=$(echo "$os_release_content" | grep -e "VERSION_ID" | cut -d '"' -f 2)
	OS_VERSION_NAME=$(echo "$os_release_content" | sed -n '1 p' | cut -d '"' -f 2)
	unset os_release_content
fi

# Classic installation
if [ "$INSTALL" = "1" ] ; then
	banner

	# Information
	echo -e "[Information]"
	echo -e "OpenStack version: ${white}OpenStack $OPENSTACK_VERSION_NAME $OPENSTACK_VERSION_NUMBER${grey}"
	echo -e "Installation type: ${white}[Single Node]${grey}"
	echo -e "Minimum services:  ${white}Keystone, Glance, Placement, Nova, Neutron, Horizon, Cinder${grey}"
	echo -e "Other services:    ${white}Kuryr, Zun${grey}"

	echo -e "\n[Prerequisite check]"

	# Disabling automatic updates & upgrades
	if [ -f /etc/apt/apt.conf.d/20auto-upgrades ] ; then
		sed -ir "s/^[#]*\s*APT::Periodic::Update-Package-Lists.*/APT::Periodic::Update-Package-Lists \"0\";/" /etc/apt/apt.conf.d/20auto-upgrades
		sed -ir "s/^[#]*\s*APT::Periodic::Unattended-Upgrade.*/APT::Periodic::Unattended-Upgrade \"0\";/" /etc/apt/apt.conf.d/20auto-upgrades
		echo -e "$green[+]$grey Automatic updates disabling Success$grey "
	fi

	# Check OS compatibility
	if [ "$OS_VERSION_NAME" = "Ubuntu" ] ; then
		if [ "$OS_VERSION_ID" = "20.04" ] ; then
			echo -e "$green[+]$grey OS is fully compatible (Ubuntu 20.04.XX)"
		else
			echo -e "$yellow[!]$grey OS may be incompatible"
			echo -e "\tNAME: $yellow$OS_VERSION_NAME$grey\n\tID: $yellow$OS_VERSION_ID$grey"
		fi
	else
		echo -e "$yellow[!]$grey OS may be incompatible"
		echo -e "\tNAME: $yellow$OS_VERSION_NAME$grey\n\tID: $yellow$OS_VERSION_ID$grey"
		echo -e "\tAn optimum OS would be: ${green}Ubuntu 20.04.2 LTS (Focal Fossa)${grey}"
	fi

	# Check Hardware ressources
	if [ -f /proc/meminfo ] ; then
		mem_ram_kb=$(cat /proc/meminfo | grep -e "MemTotal" | awk '{print $2}')

		# If RAM < 4 000 000 Ko (4Go)
		if [ $mem_ram_kb -lt 4000000 ] ; then
			echo -e "$yellow[!]$grey RAM resource may be insufficient"
			echo -e "\tRAM: $yellow$mem_ram_kb KB$grey"
		else
			echo -e "$green[+]$grey RAM resource looked right"
		fi
	fi

	if [ -f /proc/cpuinfo ] ; then
		cpus=0
		for i in $(cat /proc/cpuinfo | grep -e "cpu cores" | cut -d ':' -f 2 | sed 's/ //g'); do
			cpus=$(($cpus + $i))
		done

		# If CPUs < 2
		if [ $cpus -le 1 ] ; then
			echo -e "$yellow[!]$grey The number of CPU cores is only enough for installation"
			echo -e "\tCPU(s): $yellow$cpus$grey"
		else
			echo -e "$green[+]$grey Number of CPUs looked right"
		fi
	fi

	for i in $(df -BM | sed 's/ /_/g') ; do
		line=$(echo -e "$i" | sed 's/_/ /g')
		monted_location=$(echo -e "$line" | awk '{print $6}')

		if [ "$monted_location" = "/" ] ; then
			storage_size_available_mb=$(echo -e "$line" | awk '{print $4}' | cut -d 'M' -f 1)
		fi
	done

	# If Storage available < 10 000 Mo (10Go)   Installation take ~4Gio
	if [ $storage_size_available_mb -lt 10000 ] ; then
		echo -e "$yellow[!]$grey Storage size may be insufficient (unless the size is dynamic)"
		echo -e "\tStorage: $yellow$storage_size_available_mb MB$grey"
	else
		echo -e "$green[+]$grey Storage size looked right"
	fi

	# Check openstack exist
	if which openstack 1> /dev/null ; then
		echo -en "\rOpenStack detected, check version ..."
		openstack_current_version=$(openstack --version)
		echo -e "\r$yellow[!]$grey '$openstack_current_version' already installed, there may be collisions"
	fi

	# Check 'sdb' partition to create LVM physical volume
	if ls /dev/sdb 1> /dev/null 2>&1 ; then
		echo -e "$green[+]$grey /dev/sdb partition detected !"
	else
		echo -e "$redb[!]$grey No SDB partition detected"
		echo -e "\tNecessary to create the LVM physical volume and the LVM volume group for the cinder service"
		echo -e "\tIf you are on a VM, you can add a storage unit to solve the problem"
	fi

	echo -en "\n${white} ENTER to continue${grey}"
	read pause

	echo -e "\n[Define variables]"
	echo -e
	echo -e "  1 - Define variables for the first time or redefine them"
	echo -e "  2 - Use previously defined variables (from 'variables.txt' file)"
	echo -e

	read -n 1 -p "[?] Choose (1,2)> " define_var

	if [ $define_var -eq 2 ] ; then
		recover_variables

		# If the 1st variable 'CONTROLLER_IP' is empty (empty by default)
		if [ -z $controller_ip ] ; then
			error "La variable 'CONTROLLER_IP' n'est pas définie" "The 'CONTROLLER_IP' variable is not defined"
			exit $EXIT_FAILURE
		fi

	elif [ $define_var -eq 1 ] ; then
		echo -e "\n$white[!] If you have been installing from a virtual machine, Lab4CE recommends creating"
		echo -e "    an OpenStack internal network (e.g. 10.2.0.1/24). For this, create a new network interface$grey\n"

		# NIC Name
		nic_names=$(ip a | grep -v "lo:" | grep -e "LOWER_UP" | awk '{print $2}' | sed 's/://g')
		for nic_name in $(echo -e "$nic_names") ; do
			nic_names_tab+=("$nic_name")
		done

		# IPv4 follow NIC name order
		nic_ipv4=$(ip a | grep -v "lo:" | grep -e "LOWER_UP" -A 2 | grep -e "inet" | sed 's/ /_/g' | sed 's/____//g')
		for i in $(echo -e "$nic_ipv4") ; do
			ip_type=$(echo -e "$i" | cut -d '_' -f 1)

			if [ "$ip_type" = "inet" ] ; then
				ip_addr=$(echo -e "$i" | cut -d '_' -f 2 | cut -d '/' -f 1)
				nic_ip_tab+=("$ip_addr")

			elif [ "$ip_type" = "inet6" ] ; then
				nic_ip_tab+=("")
			fi
		done

		rows="%-2s %-2d %-15s %-25s\n"
		for i in $(seq 0 $((${#nic_names_tab[@]} - 1))) ; do
			if [ -z ${nic_ip_tab[$i]} ] ; then
				printf "$rows" "  " "$i" "-  ${nic_names_tab[$i]}"
			else
				printf "$rows" "  " "$i" "-  ${nic_names_tab[$i]}" "(${nic_ip_tab[$i]})"
			fi
		done

		# CONTROLLER_IP
		echo -e
		controller_ip=10000000
		while [ -z ${nic_names_tab[$controller_ip]} ] ; do
			read -p "[?] Select the IP adrress for the Controller> " controller_ip

			if [ -z $controller_ip ] ; then
				controller_ip=10000000
			fi
		done

		controller_nic_name=${nic_names_tab[$controller_ip]}
		sed -ir "s/^[#]*\s*CONTROLLER_NIC_NAME=.*/CONTROLLER_NIC_NAME=$controller_nic_name/" $CURRENT_PATH/variables.txt

		if [ -z ${nic_ip_tab[$controller_ip]} ] ; then
			default_controller_ip="10.2.0.1"
			read -p "[?] Choose an IPv4 for the controller, default: ${default_controller_ip} (enter for default)> " controller_ip
			controller_ip="${controller_ip:-${default_controller_ip}}"
			sed -ir "s/^[#]*\s*CONTROLLER_IP=.*/CONTROLLER_IP=$controller_ip/" $CURRENT_PATH/variables.txt
		else
			controller_ip=${nic_ip_tab[$controller_ip]}
			sed -ir "s/^[#]*\s*CONTROLLER_IP=.*/CONTROLLER_IP=$controller_ip/" $CURRENT_PATH/variables.txt
		fi

		# Passwords
		default_rabbit_pass="lab4ceRabbit"
		read -p "[?] Choose a password for RabbitMQ openstack user (enter for default)> " rabbit_pass
		rabbit_pass="${rabbit_pass:-${default_rabbit_pass}}"
		sed -ir "s/^[#]*\s*RABBIT_PASS=.*/RABBIT_PASS=$rabbit_pass/" $CURRENT_PATH/variables.txt

		default_metadata_secret="lab4ceMetadata"
		read -p "[?] Choose a secret for the metadata proxy (enter for default)> " metadata_secret
		metadata_secret="${metadata_secret:-${default_metadata_secret}}"
		sed -ir "s/^[#]*\s*METADATA_SECRET=.*/METADATA_SECRET=$metadata_secret/" $CURRENT_PATH/variables.txt

		default_dash_dbpass=""
		read -p "[?] Specify the Database password for the Dashboard (enter for default)> " dash_dbpass
		dash_dbpass="${dash_dbpass:-${default_dash_dbpass}}"
		sed -ir "s/^[#]*\s*DASH_DBPASS=.*/DASH_DBPASS=$dash_dbpass/" $CURRENT_PATH/variables.txt

		default_mysql_superuser_dbpass="Lab4ceSQL"
		read -p "[?] Specify the MySQL superuser password (enter for default)> " mysql_superuser_dbpass
		mysql_superuser_dbpass="${mysql_superuser_dbpass:-${default_mysql_superuser_dbpass}}"
		sed -ir "s/^[#]*\s*MYSQL_SUPERUSER_DBPASS=.*/MYSQL_SUPERUSER_DBPASS=$mysql_superuser_dbpass/" $CURRENT_PATH/variables.txt

		default_keystone_dbpass="lab4ceSQLKeystone"
		read -p "[?] Specify the Database password for Identity service (enter for default)> " keystone_dbpass
		keystone_dbpass="${keystone_dbpass:-${default_keystone_dbpass}}"
		sed -ir "s/^[#]*\s*KEYSTONE_DBPASS=.*/KEYSTONE_DBPASS=$keystone_dbpass/" $CURRENT_PATH/variables.txt

		default_glance_dbpass="lab4ceSQLGlance"
		read -p "[?] Specify the Database password for Image service (enter for default)> " glance_dbpass
		glance_dbpass="${glance_dbpass:-${default_glance_dbpass}}"
		sed -ir "s/^[#]*\s*GLANCE_DBPASS=.*/GLANCE_DBPASS=$glance_dbpass/" $CURRENT_PATH/variables.txt

		default_placement_dbpass="lab4ceSQLPlacement"
		read -p "[?] Specify the Database password for Placement service (enter for default)> " placement_dbpass
		placement_dbpass="${placement_dbpass:-${default_placement_dbpass}}"
		sed -ir "s/^[#]*\s*PLACEMENT_DBPASS=.*/PLACEMENT_DBPASS=$placement_dbpass/" $CURRENT_PATH/variables.txt

		default_nova_dbpass="lab4ceSQLNova"
		read -p "[?] Specify the Database password for Compute service (enter for default)> " nova_dbpass
		nova_dbpass="${nova_dbpass:-${default_nova_dbpass}}"
		sed -ir "s/^[#]*\s*NOVA_DBPASS=.*/NOVA_DBPASS=$nova_dbpass/" $CURRENT_PATH/variables.txt

		default_neutron_dbpass="lab4ceSQLNeutron"
		read -p "[?] Specify the Database password for Networking service (enter for default)> " neutron_dbpass
		neutron_dbpass="${neutron_dbpass:-${default_neutron_dbpass}}"
		sed -ir "s/^[#]*\s*NEUTRON_DBPASS=.*/NEUTRON_DBPASS=$neutron_dbpass/" $CURRENT_PATH/variables.txt

		default_cinder_dbpass="lab4ceSQLCinder"
		read -p "[?] Specify the Database password for the Block Storage service (enter for default)> " cinder_dbpass
		cinder_dbpass="${cinder_dbpass:-${default_cinder_dbpass}}"
		sed -ir "s/^[#]*\s*CINDER_DBPASS=.*/CINDER_DBPASS=$cinder_dbpass/" $CURRENT_PATH/variables.txt

		default_zun_dbpass="lab4ceSQLZun"
		read -p "[?] Specify the Database password for Containers service (enter for default)> " zun_dbpass
		zun_dbpass="${zun_dbpass:-${default_zun_dbpass}}"
		sed -ir "s/^[#]*\s*ZUN_DBPASS=.*/ZUN_DBPASS=$zun_dbpass/" $CURRENT_PATH/variables.txt

		default_admin_pass="lab4ce-admin"
		read -p "[?] Specify the Password for user admin (enter for default)> " admin_pass
		admin_pass="${admin_pass:-${default_admin_pass}}"
		sed -ir "s/^[#]*\s*ADMIN_PASS=.*/ADMIN_PASS=$admin_pass/" $CURRENT_PATH/variables.txt

		default_glance_pass="lab4ce-glance"
		read -p "[?] Specify the Password for Image service user glance (enter for default)> " glance_pass
		glance_pass="${glance_pass:-${default_glance_pass}}"
		sed -ir "s/^[#]*\s*GLANCE_PASS=.*/GLANCE_PASS=$glance_pass/" $CURRENT_PATH/variables.txt

		default_placement_pass="lab4ce-placement"
		read -p "[?] Specify the Password for Placement service user placement (enter for default)> " placement_pass
		placement_pass="${placement_pass:-${default_placement_pass}}"
		sed -ir "s/^[#]*\s*PLACEMENT_PASS=.*/PLACEMENT_PASS=$placement_pass/" $CURRENT_PATH/variables.txt

		default_nova_pass="lab4ce-nova"
		read -p "[?] Specify the Password for Compute service user nova (enter for default)> " nova_pass
		nova_pass="${nova_pass:-${default_nova_pass}}"
		sed -ir "s/^[#]*\s*NOVA_PASS=.*/NOVA_PASS=$nova_pass/" $CURRENT_PATH/variables.txt

		default_neutron_pass="lab4ce-neutron"
		read -p "[?] Specify the Password for Networking service user neutron (enter for default)> " neutron_pass
		neutron_pass="${neutron_pass:-${default_neutron_pass}}"
		sed -ir "s/^[#]*\s*NEUTRON_PASS=.*/NEUTRON_PASS=$neutron_pass/" $CURRENT_PATH/variables.txt

		default_cinder_pass="lab4ce-cinder"
		read -p "[?] Specify the Password for Block Storage service user cinder (enter for default)> " cinder_pass
		cinder_pass="${cinder_pass:-${default_cinder_pass}}"
		sed -ir "s/^[#]*\s*CINDER_PASS=.*/CINDER_PASS=$cinder_pass/" $CURRENT_PATH/variables.txt

		default_kuryr_pass="lab4ce-kuryr"
		read -p "[?] Specify the Password for user kuryr (enter for default)> " kuryr_pass
		kuryr_pass="${kuryr_pass:-${default_kuryr_pass}}"
		sed -ir "s/^[#]*\s*KURYR_PASS=.*/KURYR_PASS=$kuryr_pass/" $CURRENT_PATH/variables.txt

		default_zun_pass="lab4ce-zun"
		read -p "[?] Specify the Password for Containers user zun (enter for default)> " zun_pass
		zun_pass="${zun_pass:-${default_zun_pass}}"
		sed -ir "s/^[#]*\s*ZUN_PASS=.*/ZUN_PASS=$zun_pass/" $CURRENT_PATH/variables.txt

		if [ -f $CURRENT_PATH/variables.txtr ] ; then
			rm -f $CURRENT_PATH/variables.txtr
		fi

		recover_variables
	else
		error "L'option choisie n'existe pas" "The chosen option does not exist"
		exit $EXIT_FAILURE
	fi

	echo -e "\n[Network]"

	if ! which netplan 1> /dev/null ; then
		apt_package netplan.io
	fi

	if [ ! -d /etc/netplan ] ; then
		error "Le répertoire /etc/netplan/ n'existe pas" "The /etc/netplan/ directory does not exist"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey '/etc/netplan' directory exist"
	fi

	if [ -z $controller_ip -o -z $controller_nic_name ] ; then
		error "L'adresse IP ou l'interface réseau du controller n'est pas définie" "The IP address or the Controller network interface is not defined"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey variables 'CONTROLLER_IP' and 'CONTROLLER_NIC_NAME' exist"
	fi

	check_ip $controller_ip
	if [ $? -ne 0 ] ; then
		error "Le format de l'adresse IP du controller est incorrect" "The format of the Controller IP address is incorrect"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey 'CONTROLLER_IP' format is correct"
	fi

	hosts_loopback=$(sed -rn 's/^127.0.1.1 ([^\n]+)$/\1/p' /etc/hosts)
	hosts_controller=$(cat /etc/hosts | grep -e "$controller_ip" | cut -d ' ' -f 2)
	
	if [ "$hosts_loopback" != "lab4ce" ] ; then
		echo -e "\n#Lab4CE Config" >> /etc/hosts
		echo -e "127.0.1.1 lab4ce" >> /etc/hosts
	fi

	if [ "$hosts_controller" != "controller" ] ; then
		echo -e "\n#Lab4CE Config" >> /etc/hosts
		echo -e "${controller_ip} controller" >> /etc/hosts
	fi

	echo -e "$green[+]$grey /etc/hosts defined"

	if [ ! -f /etc/netplan/br-ex.yaml ] ; then
		cp $CURRENT_PATH/network/br-ex.yaml /etc/netplan/
	fi

	if [ ! -f /etc/netplan/controller_nic.yaml ] ; then
		cp $CURRENT_PATH/network/controller_nic.yaml /etc/netplan/
	fi

	if [ ! -f /etc/netplan/br-ex.yaml -o ! -f /etc/netplan/controller_nic.yaml ] ; then
		error "La copie des fichiers de la configuration réseau a échoué" "Copying the network configuration files failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying the network configuration files Success"
	fi

	sed -ir "s/^[#]*\s*NIC_NAME.*/  ${controller_nic_name}:/" /etc/netplan/controller_nic.yaml
	sed -ir "s/^[#]*\s*- IP_CIDR.*/    - ${controller_ip}\/24/" /etc/netplan/controller_nic.yaml
	echo -e "$green[+]$grey Define the static Controller IP address -> $controller_ip/24"

	if [ -f /etc/netplan/controller_nic.yamlr ] ; then
		rm -f /etc/netplan/controller_nic.yamlr
	fi

	netplan apply
	echo -e "$green[+]$grey Apply updates"

	service_restart systemd-networkd

	echo -e "\n[NTP Server]"

	apt_package chrony

	service_restart chrony

	systemctl enable chrony.service 1> /dev/null 2>&1
	echo -e "$green[+]$grey Enable 'chrony' at the start of the computer"

	echo -e "\n[OpenStack Dependencies]"

	echo -en "\rInstallation of 'cloud-archive:victoria' in progress ..."
	add-apt-repository -y cloud-archive:victoria 1> /dev/null 2>&1
	echo -e "\r$green[+]$grey Installation of 'cloud-archive:victoria' Success    "

	apt_package python3-openstackclient python3-pip

	echo -en "\rInstallation of 'python-openstackclient' in progress ..."
	pip3_package python-openstackclient
	echo -e "\r$green[+]$grey Installation of 'python-openstackclient' Success    "

	echo -e "\n[MariaDB]"

	apt_package mariadb-server python3-pymysql

	cp $CURRENT_PATH/mysql/99-openstack.cnf /etc/mysql/mariadb.conf.d/

	if [ ! -f /etc/mysql/mariadb.conf.d/99-openstack.cnf ] ; then
		error "La copie du fichier '99-openstack.cnf' a échoué" "The copy of file '99-openstack.cnf' failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying the configuration file Success"
	fi

	sed -ir "s/^[#]*\s*bind-address = .*/bind-address = ${controller_ip}/" /etc/mysql/mariadb.conf.d/99-openstack.cnf
	echo -e "$green[+]$grey Setting the Controller IP address for OpenStack configuration file"

	if [ -f /etc/mysql/mariadb.conf.d/99-openstack.cnfr ] ; then
		rm -f /etc/mysql/mariadb.conf.d/99-openstack.cnfr
	fi

	service_restart mysql

	if [ ! -x $CURRENT_PATH/mysql/mysql_secure_installation ] ; then
		chmod +x $CURRENT_PATH/mysql/mysql_secure_installation
	fi
	. $CURRENT_PATH/mysql/mysql_secure_installation

	if [ "$mysql_superuser_dbpass" = "" ] ; then
		error "Le mot de passe du superutilisateur est vide" "The password of MySQL superuser is empty"
		exit $EXIT_FAILURE
	fi

	esc_mysql_superuser_dbpass=$(basic_single_escape $mysql_superuser_dbpass)
	mysql -u root -e "UPDATE mysql.user SET Password=PASSWORD('${esc_mysql_superuser_dbpass}') WHERE User='root';"
	echo -e "$green[+]$grey Setting MySQL superuser password"

	echo -en "\rCreating databases for different services  ..."
	cp $CURRENT_PATH/mysql/databases.sql $CURRENT_PATH/mysql/databases.sql.temp
	
	sed -ir "s/^[#]*\s*SET @KEYSTONE_DBPASS.*/SET @KEYSTONE_DBPASS:='${keystone_dbpass}';/" $CURRENT_PATH/mysql/databases.sql.temp
	sed -ir "s/^[#]*\s*SET @GLANCE_DBPASS.*/SET @GLANCE_DBPASS:='${glance_dbpass}';/" $CURRENT_PATH/mysql/databases.sql.temp
	sed -ir "s/^[#]*\s*SET @PLACEMENT_DBPASS.*/SET @PLACEMENT_DBPASS:='${placement_dbpass}';/" $CURRENT_PATH/mysql/databases.sql.temp
	sed -ir "s/^[#]*\s*SET @NOVA_DBPASS.*/SET @NOVA_DBPASS:='${nova_dbpass}';/" $CURRENT_PATH/mysql/databases.sql.temp
	sed -ir "s/^[#]*\s*SET @NEUTRON_DBPASS.*/SET @NEUTRON_DBPASS:='${neutron_dbpass}';/" $CURRENT_PATH/mysql/databases.sql.temp
	sed -ir "s/^[#]*\s*SET @CINDER_DBPASS.*/SET @CINDER_DBPASS:='${cinder_dbpass}';/" $CURRENT_PATH/mysql/databases.sql.temp
	sed -ir "s/^[#]*\s*SET @ZUN_DBPASS.*/SET @ZUN_DBPASS:='${zun_dbpass}';/" $CURRENT_PATH/mysql/databases.sql.temp

	mysql -u root --password='$esc_mysql_superuser_dbpass' < $CURRENT_PATH/mysql/databases.sql.temp 1> /dev/null 2>&1

	check_mysql_db_created keystone glance placement nova_api nova nova_cell0 neutron cinder zun
	echo -e "\r$green[+]$grey Creating databases Success                "

	if [ -f $CURRENT_PATH/mysql/databases.sql.temp ] ; then
		rm -f $CURRENT_PATH/mysql/databases.sql.temp*
	fi

	check_mysql_user_created keystone glance placement nova neutron cinder zun
	echo -e "$green[+]$grey Creating MySQL users Success"

	echo -e "\n[RabbitMQ]"

	apt_package rabbitmq-server

	if [ -z $rabbit_pass ] ; then
		error "'RABBIT_PASS' n'est pas définie" "'RABBIT_PASS' is not defined"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey variable 'RABBIT_PASS' exist"
	fi

	rabbitmqctl add_user openstack $rabbit_pass 1> /dev/null 2>&1
	echo -e "$green[+]$grey Create RabbitMQ 'openstack' user"
	rabbitmqctl set_permissions openstack ".*" ".*" ".*" 1> /dev/null 2>&1
	echo -e "$green[+]$grey Give OpenStack permissions"

	echo -e "\n[Memcached]"

	apt_package memcached python3-memcache

	if [ ! -f /etc/memcached.conf ] ; then
		error "Le fichier de configuration '/etc/memcached.conf' est introuvable" "The '/etc/memcached.conf' configuration file can not be found"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Memcached configuration file exist"
	fi

	sed -ir "s/^[#]*\s*-l 127.0.0.1.*/-l ${controller_ip}/" /etc/memcached.conf
	echo -e "$green[+]$grey Setting IP address to listen on"

	service_restart memcached

	echo -e "\n[Etcd]"

	apt_package etcd

	if [ ! -f /etc/default/etcd ] ; then
		error "Le fichier de configuration '/etc/default/etcd' est introuvable" "The '/etc/default/etcd' configuration file can not be found"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Etcd configuration file exist"
	fi

	etcd_name=$(sed -rn 's/^ETCD_NAME=([^\n]+)$/\1/p' /etc/default/etcd | sed 's/"//g')

	if [ "$etcd_name" = "controller" ] ; then
		echo -e "$green[+]$grey Etcd configuration already defined"
	else
		echo "ETCD_NAME=\"controller\"" >> /etc/default/etcd
		echo "ETCD_DATA_DIR=\"/var/lib/etcd\"" >> /etc/default/etcd
		echo "ETCD_INITIAL_CLUSTER_STATE=\"new\"" >> /etc/default/etcd
		echo "ETCD_INITIAL_CLUSTER_TOKEN=\"etcd-cluster-01\"" >> /etc/default/etcd
		echo "ETCD_INITIAL_CLUSTER=\"controller=http://${controller_ip}:2380\"" >> /etc/default/etcd
		echo "ETCD_INITIAL_ADVERTISE_PEER_URLS=\"http://${controller_ip}:2380\"" >> /etc/default/etcd
		echo "ETCD_ADVERTISE_CLIENT_URLS=\"http://${controller_ip}:2379\"" >> /etc/default/etcd
		echo "ETCD_LISTEN_PEER_URLS=\"http://0.0.0.0:2380\"" >> /etc/default/etcd
		echo "ETCD_LISTEN_CLIENT_URLS=\"http://${controller_ip}:2379\"" >> /etc/default/etcd

		echo -e "$green[+]$grey Etcd configuration defined"
	fi

	service_restart etcd

	systemctl enable etcd 1> /dev/null 2>&1
	echo -e "$green[+]$grey Enable 'etcd' at the start of the computer"

	# Start the first service installation 
	install_keystone

	exit $EXIT_SUCCESS
else
	error "Le paramètre '--install' est manquant" "The '--install' parameter is missing"
	exit $EXIT_FAILURE
fi

exit $EXIT_SUCCESS