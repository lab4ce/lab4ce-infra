#!/bin/bash
#coding:utf-8
#Licence: CeCILL-B
#Project: Lab4CE
#Service: Nova

function install_nova(){

	check_point="${FUNCNAME[0]}"

	echo -e "\n[Nova - Compute service]"

	check_env_var_defined

	if [ -z $nova_pass ] ; then
		error "La variable 'NOVA_PASS' n'est pas définie" "The variable 'NOVA_PASS' is not defined"
		exit $EXIT_FAILURE
	fi
	esc_nova_pass=$(basic_single_escape $nova_pass)

	echo -en "\rCreate the service credentials ..."
	[[ $(openstack user create --domain default --password "${esc_nova_pass}" nova) ]] && echo -en "\rCreate the service credentials [1/3]" || error "Échec lors de la création du service d'identifiants [1/3]" "Failed when creating the identifier service [1/3]"
	openstack role add --project service --user nova admin
	echo -en "\rCreate the service credentials [2/3]"
	[[ $(openstack service create --name nova --description "OpenStack Compute" compute) ]] && echo -e "\r$green[+]$grey Service credentials Successfully created" || error "Échec lors de la création du service d'identifiants [3/3]" "Failed when creating the identifier service [3/3]"

	echo -en "\rCreate the Nova service API endpoints ..."
	[[ $(openstack endpoint create --region RegionOne compute public http://controller:8774/v2.1) ]] && echo -en "\rCreate the Nova service API endpoints [1/3]" || error "Échec lors de la création du point de sortie du service API Nova [1/3]" "Failed when creating the endpoint of the Nova API Service [1/3]"
	[[ $(openstack endpoint create --region RegionOne compute internal http://controller:8774/v2.1) ]] && echo -en "\rCreate the Nova service API endpoints [2/3]" || error "Échec lors de la création du point de sortie du service API Nova [2/3]" "Failed when creating the endpoint of the Nova API Service [2/3]"
	[[ $(openstack endpoint create --region RegionOne compute admin http://controller:8774/v2.1) ]] && echo -e "\r$green[+]$grey Nova service API endpoints Successfully created" || error "Échec lors de la création du point de sortie du service API Nova [3/3]" "Failed when creating the endpoint of the Nova API Service [3/3]"

	apt_package nova-api nova-conductor nova-novncproxy nova-scheduler nova-compute

	if [ ! -f /etc/nova/nova.conf ] ; then
		error "Le fichier de configuration de nova n'existe pas" "Nova configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/nova/nova.conf /etc/nova/nova.conf.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'nova.conf.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/nova.conf /etc/nova/

	if [ ! -f /etc/nova/nova.conf ] ; then
		error "La copie du fichier de configuration a échoué" "Copying configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying configuration file Success"
	fi

	if [ -z $nova_dbpass ] ; then
		error "La variable 'NOVA_DBPASS' n'est pas définie" "The variable 'NOVA_DBPASS' is not defined"
		exit $EXIT_FAILURE
	fi

	if [ -z $rabbit_pass ] ; then
		error "La variable 'RABBIT_PASS' n'est pas définie" "The variable 'RABBIT_PASS' is not defined"
		exit $EXIT_FAILURE
	fi

	if [ -z $metadata_secret ] ; then
		error "La variable 'METADATA_SECRET' n'est pas définie" "The variable 'METADATA_SECRET' is not defined"
		exit $EXIT_FAILURE
	fi

	if [ -z $controller_ip ] ; then
		error "La variable 'CONTROLLER_IP' n'est pas définie" "The variable 'CONTROLLER_IP' is not defined"
		exit $EXIT_FAILURE
	fi

	if [ -z $neutron_pass ] ; then
		error "La variable 'NEUTRON_PASS' n'est pas définie" "The variable 'NEUTRON_PASS' is not defined"
		exit $EXIT_FAILURE
	fi

	if [ -z $placement_pass ] ; then
		error "La variable 'PLACEMENT_PASS' n'est pas définie" "The variable 'PLACEMENT_PASS' is not defined"
		exit $EXIT_FAILURE
	fi

	sed -ir "s/^[#]*\s*connection = mysql+pymysql:\/\/nova:NOVA_DBPASS@controller\/nova_api.*/connection = mysql+pymysql:\/\/nova:${nova_dbpass}@controller\/nova_api/" /etc/nova/nova.conf
	sed -ir "s/^[#]*\s*connection = mysql+pymysql:\/\/nova:NOVA_DBPASS@controller\/nova.*/connection = mysql+pymysql:\/\/nova:${nova_dbpass}@controller\/nova/" /etc/nova/nova.conf

	sed -ir "s/^[#]*\s*transport_url = rabbit:\/\/openstack:.*/transport_url = rabbit:\/\/openstack:${rabbit_pass}@controller:5672\//" /etc/nova/nova.conf
	sed -ir "s/^[#]*\s*metadata_proxy_shared_secret = .*/metadata_proxy_shared_secret = ${metadata_secret}/" /etc/nova/nova.conf
	sed -ir "s/^[#]*\s*my_ip = CONTROLLER_IP.*/my_ip = ${controller_ip}/" /etc/nova/nova.conf

	sed -ir "s/^[#]*\s*password = NOVA_PASS.*/password = ${nova_pass}/" /etc/nova/nova.conf
	sed -ir "s/^[#]*\s*password = NEUTRON_PASS.*/password = ${neutron_pass}/" /etc/nova/nova.conf
	sed -ir "s/^[#]*\s*password = PLACEMENT_PASS.*/password = ${placement_pass}/" /etc/nova/nova.conf

	echo -e "$green[+]$grey Setting 'nova.conf' Success"

	if [ -f /etc/nova/nova.confr ] ; then
		rm -f /etc/nova/nova.confr
	fi

	echo -en "\rPopulate the 'nova_api' database ..."
	su -s /bin/sh -c "nova-manage api_db sync" nova 1> /dev/null 2>&1
	check_populate nova_api

	echo -en "\rPopulate 'nova_cell0' and 'nova' databases ..."
	su -s /bin/sh -c "nova-manage cell_v2 map_cell0" nova 1> /dev/null 2>&1
	su -s /bin/sh -c "nova-manage cell_v2 create_cell --name=cell1 --verbose" nova 1> /dev/null 2>&1
	su -s /bin/sh -c "nova-manage db sync" nova 1> /dev/null 2>&1
	check_populate nova_cell0 nova

	echo -en "\rVerify 'nova_cell0' and 'nova_cell1' are registered correctly ..."
	if ! su -s /bin/sh -c "nova-manage cell_v2 list_cells" nova 1> /dev/null 2>&1 ; then
		error "Échec de la vérification" "Failed to check"
		exit $EXIT_FAILURE
	else
		echo -e "\r$green[+]$grey Verify 'nova_cell0' and 'nova_cell1' are registered correctly Success"
	fi

	service_restart nova-api nova-scheduler nova-conductor nova-novncproxy

	if [ ! -f /etc/nova/nova-compute.conf ] ; then
		error "Le fichier de configuration de nova compute n'existe pas" "Nova compute configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/nova/nova-compute.conf /etc/nova/nova-compute.conf.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'nova-compute.conf.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/nova-compute.conf /etc/nova/

	if [ ! -f /etc/nova/nova-compute.conf ] ; then
		error "La copie du fichier de configuration a échoué" "Copying configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying configuration file Success"
	fi

	echo -en "\rCheck supports hardware acceleration for virtual machines ..."
	if [ $(egrep -c '(vmx|svm)' /proc/cpuinfo) -eq 0 ] ; then
		echo -e "\r$green[+]$grey Compute node supports hardware acceleration for virtual machines : NO"

		sed -ir "s/^[#]*\s*virt_type=.*/virt_type=qemu/" /etc/nova/nova-compute.conf

		echo -e "$green[+]$grey Setting 'nova-compute.conf' Success"

		if [ -f /etc/nova/nova-compute.confr ] ; then
			rm -f /etc/nova/nova-compute.confr
		fi

	else
		echo -e "\r$green[+]$grey Compute node supports hardware acceleration for virtual machines : YES"
	fi

	service_restart nova-compute

	echo -en "\rConfirm there are compute hosts in the database ..."
	sleep 15
	if [ "$(openstack compute service list --service nova-compute -f value | grep -e "nova-compute" | cut -d ' ' -f 5)" != "enabled" ] ; then
		error "Aucun 'Compute Host' n'a été trouvé dans la base de données" "None 'Compute Host' was found in the database"
		exit $EXIT_FAILURE
	else
		echo -e "\r$green[+]$grey Confirm there are compute hosts in the database Success"
	fi

	echo -en "\rDiscover compute hosts ..."
	if ! su -s /bin/sh -c "nova-manage cell_v2 discover_hosts --verbose" nova 1> /dev/null 2>&1 ; then
		error "Aucun 'Compute Host' n'a été trouvé" "None 'Compute Host' was found"
		exit $EXIT_FAILURE
	else
		echo -e "\r$green[+]$grey Discover compute hosts Success"
	fi

	# Installing the following service
	install_neutron
}
