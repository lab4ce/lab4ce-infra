#!/bin/bash
#coding:utf-8
#Licence: CeCILL-B
#Project: Lab4CE
#Service: Placement

function install_placement(){

	check_point="${FUNCNAME[0]}"

	echo -e "\n[Placement - Placement service]"

	check_env_var_defined

	if [ -z $placement_pass ] ; then
		error "La variable 'PLACEMENT_PASS' n'est pas définie" "The variable 'PLACEMENT_PASS' is not defined"
		exit $EXIT_FAILURE
	fi
	esc_placement_pass=$(basic_single_escape $placement_pass)

	echo -en "\rCreate the service credentials ..."
	[[ $(openstack user create --domain default --password "${esc_placement_pass}" placement) ]] && echo -en "\rCreate the service credentials [1/3]" || error "Échec lors de la création du service d'identifiants [1/3]" "Failed when creating the identifier service [1/3]"
	openstack role add --project service --user placement admin
	echo -en "\rCreate the service credentials [2/3]"
	[[ $(openstack service create --name placement --description "OpenStack Placement" placement) ]] && echo -e "\r$green[+]$grey Service credentials Successfully created" || error "Échec lors de la création du service d'identifiants [3/3]" "Failed when creating the identifier service [3/3]"

	echo -en "\rCreate the Placement service API endpoints ..."
	[[ $(openstack endpoint create --region RegionOne placement public http://controller:8778) ]] && echo -en "\rCreate the Placement service API endpoints [1/3]" || error "Échec lors de la création du point de sortie du service API Placement [1/3]" "Failed when creating the endpoint of the Placement API Service [1/3]"
	[[ $(openstack endpoint create --region RegionOne placement internal http://controller:8778) ]] && echo -en "\rCreate the Placement service API endpoints [2/3]" || error "Échec lors de la création du point de sortie du service API Placement [2/3]" "Failed when creating the endpoint of the Placement API Service [2/3]"
	[[ $(openstack endpoint create --region RegionOne placement admin http://controller:8778) ]] && echo -e "\r$green[+]$grey Placement service API endpoints Successfully created" || error "Échec lors de la création du point de sortie du service API Placement [3/3]" "Failed when creating the endpoint of the Placement API Service [3/3]"

	pip3_package openstack-placement pymysql osc-placement uwsgi

	apt_package placement-api

	if [ ! -f /etc/placement/placement.conf ] ; then
		error "Le fichier de configuration de placement n'existe pas" "Placement configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/placement/placement.conf /etc/placement/placement.conf.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'placement.conf.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/placement.conf /etc/placement/

	if [ ! -f /etc/placement/placement.conf ] ; then
		error "La copie du fichier de configuration a échoué" "Copying configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying configuration file Success"
	fi

	if [ -z $placement_dbpass ] ; then
		error "La variable 'PLACEMENT_DBPASS' n'est pas définie" "The variable 'PLACEMENT_DBPASS' is not defined"
		exit $EXIT_FAILURE
	fi

	sed -ir "s/^[#]*\s*connection = mysql+pymysql.*/connection = mysql+pymysql:\/\/placement:${placement_dbpass}@controller\/placement/" /etc/placement/placement.conf
	sed -ir "s/^[#]*\s*password = .*/password = ${placement_pass}/" /etc/placement/placement.conf
	echo -e "$green[+]$grey Setting 'placement.conf' Success"

	if [ -f /etc/placement/placement.confr ] ; then
		rm -f /etc/placement/placement.confr
	fi

	echo -en "\rPopulate the 'placement' database ..."
	su -s /bin/sh -c "placement-manage db sync" placement 1> /dev/null 2>&1
	check_populate placement

	service_restart apache2

	echo -en "\rCheck status to make sure everything is in order ..."
	placement-status upgrade check 1> /dev/null 2>&1
	echo -e "\r$green[+]$grey Check status to make sure everything is in order Success"

	echo -en "\rList available resource classes ..."
	openstack --os-placement-api-version 1.2 resource class list --sort-column name 1> /dev/null 2>&1
	echo -en "\rList available traits ...          "
	openstack --os-placement-api-version 1.6 trait list --sort-column name 1> /dev/null 2>&1
	echo -e "\r$green[+]$grey Resource classes and traits are available"

	# Installing the following service
	install_nova
}
