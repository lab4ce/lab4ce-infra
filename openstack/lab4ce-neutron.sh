#!/bin/bash
#coding:utf-8
#Licence: CeCILL-B
#Project: Lab4CE
#Service: Neutron

function install_neutron(){

	check_point="${FUNCNAME[0]}"

	echo -e "\n[Neutron - Networking]"

	check_env_var_defined

	if [ -z $neutron_pass ] ; then
		error "La variable 'NEUTRON_PASS' n'est pas définie" "The variable 'NEUTRON_PASS' is not defined"
		exit $EXIT_FAILURE
	fi
	esc_neutron_pass=$(basic_single_escape $neutron_pass)

	echo -en "\rCreate the service credentials ..."
	[[ $(openstack user create --domain default --password "${esc_neutron_pass}" neutron) ]] && echo -en "\rCreate the service credentials [1/3]" || error "Échec lors de la création du service d'identifiants [1/3]" "Failed when creating the identifier service [1/3]"
	openstack role add --project service --user neutron admin
	echo -en "\rCreate the service credentials [2/3]"
	[[ $(openstack service create --name neutron --description "OpenStack Networking" network) ]] && echo -e "\r$green[+]$grey Service credentials Successfully created" || error "Échec lors de la création du service d'identifiants [3/3]" "Failed when creating the identifier service [3/3]"

	echo -en "\rCreate the Neutron service API endpoints ..."
	[[ $(openstack endpoint create --region RegionOne network public http://controller:9696) ]] && echo -en "\rCreate the Neutron service API endpoints [1/3]" || error "Échec lors de la création du point de sortie du service API Neutron [1/3]" "Failed when creating the endpoint of the Neutron API Service [1/3]"
	[[ $(openstack endpoint create --region RegionOne network internal http://controller:9696) ]] && echo -en "\rCreate the Neutron service API endpoints [2/3]" || error "Échec lors de la création du point de sortie du service API Neutron [2/3]" "Failed when creating the endpoint of the Neutron API Service [2/3]"
	[[ $(openstack endpoint create --region RegionOne network admin http://controller:9696) ]] && echo -e "\r$green[+]$grey Neutron service API endpoints Successfully created" || error "Échec lors de la création du point de sortie du service API Neutron [3/3]" "Failed when creating the endpoint of the Neutron API Service [3/3]"

	echo -e "$white[i]$grey Networking Option 2: Self-service networks"
	apt_package neutron-server neutron-plugin-ml2 neutron-l3-agent neutron-dhcp-agent neutron-metadata-agent openvswitch-switch neutron-openvswitch-agent python3-openvswitch

	if [ ! -f /etc/neutron/neutron.conf ] ; then
		error "Le fichier de configuration de neutron n'existe pas" "Neutron configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/neutron/neutron.conf /etc/neutron/neutron.conf.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'neutron.conf.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/neutron.conf /etc/neutron/

	if [ ! -f /etc/neutron/neutron.conf ] ; then
		error "La copie du fichier de configuration a échoué" "Copying configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying configuration file Success"
	fi

	if [ -z $neutron_dbpass ] ; then
		error "La variable 'NEUTRON_DBPASS' n'est pas définie" "The variable 'NEUTRON_DBPASS' is not defined"
		exit $EXIT_FAILURE
	fi

	if [ -z $rabbit_pass ] ; then
		error "La variable 'RABBIT_PASS' n'est pas définie" "The variable 'RABBIT_PASS' is not defined"
		exit $EXIT_FAILURE
	fi

	if [ -z $nova_pass ] ; then
		error "La variable 'NOVA_PASS' n'est pas définie" "The variable 'NOVA_PASS' is not defined"
		exit $EXIT_FAILURE
	fi

	sed -ir "s/^[#]*\s*transport_url = rabbit:\/\/openstack:.*/transport_url = rabbit:\/\/openstack:${rabbit_pass}@controller/" /etc/neutron/neutron.conf
	sed -ir "s/^[#]*\s*connection = mysql+pymysql.*/connection = mysql+pymysql:\/\/neutron:${neutron_dbpass}@controller\/neutron/" /etc/neutron/neutron.conf

	sed -ir "s/^[#]*\s*password = NEUTRON_PASS.*/password = ${neutron_pass}/" /etc/neutron/neutron.conf
	sed -ir "s/^[#]*\s*password = NOVA_PASS.*/password = ${nova_pass}/" /etc/neutron/neutron.conf

	echo -e "$green[+]$grey Setting 'neutron.conf' Success"

	if [ -f /etc/neutron/neutron.confr ] ; then
		rm -f /etc/neutron/neutron.confr
	fi

	if [ ! -f /etc/neutron/plugins/ml2/ml2_conf.ini ] ; then
		error "Le fichier de configuration du plug-in 'ml2' n'existe pas" "'ml2' plug-in configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugins/ml2/ml2_conf.ini.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'ml2_conf.ini.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/ml2_conf.ini /etc/neutron/plugins/ml2/

	if [ ! -f /etc/neutron/plugins/ml2/ml2_conf.ini ] ; then
		error "La copie du fichier de configuration 'ml2' a échoué" "Copying 'ml2' configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying 'ml2' configuration file Success"
	fi

	sysctl_conf=('net.ipv4.ip_forward' 'net.ipv4.conf.all.rp_filter' 'net.ipv4.conf.default.rp_filter' 'net.bridge.bridge-nf-call-iptables' 'net.bridge.bridge-nf-call-ip6tables')

	for i in ${sysctl_conf[@]} ; do
		sysctl -q ${i}=1
	done
	sysctl -p 1> /dev/null 2>&1

	for i in ${sysctl_conf[@]} ; do
		if [ $(sysctl -q ${i} | cut -d ' ' -f 3) -ne 1 ] ; then
			error "sysctl '${i}' n'est pas égal à 1" "sysctl '${i}' not egal 1"
		fi
	done
	echo -e "$green[+]$grey sysctl has been setted to kernel supports network bridge filters"

	if [ ! -f /etc/neutron/l3_agent.ini ] ; then
		error "Le fichier de configuration du plug-in 'l3_agent' n'existe pas" "'l3_agent' plug-in configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/neutron/l3_agent.ini /etc/neutron/l3_agent.ini.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'l3_agent.ini.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/l3_agent.ini /etc/neutron/

	if [ ! -f /etc/neutron/l3_agent.ini ] ; then
		error "La copie du fichier de configuration 'l3_agent' a échoué" "Copying 'l3_agent' configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying 'l3_agent' configuration file Success"
	fi

	if [ ! -f /etc/neutron/dhcp_agent.ini ] ; then
		error "Le fichier de configuration du plug-in 'dhcp_agent' n'existe pas" "'dhcp_agent' plug-in configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/neutron/dhcp_agent.ini /etc/neutron/dhcp_agent.ini.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'dhcp_agent.ini.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/dhcp_agent.ini /etc/neutron/

	if [ ! -f /etc/neutron/dhcp_agent.ini ] ; then
		error "La copie du fichier de configuration 'dhcp_agent' a échoué" "Copying 'dhcp_agent' configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying 'dhcp_agent' configuration file Success"
	fi

	if [ ! -f /etc/neutron/plugins/ml2/openvswitch_agent.ini ] ; then
		error "Le fichier de configuration du plug-in 'openvswitch_agent' n'existe pas" "'openvswitch_agent' plug-in configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/neutron/plugins/ml2/openvswitch_agent.ini /etc/neutron/plugins/ml2/openvswitch_agent.ini.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'openvswitch_agent.ini.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/openvswitch_agent.ini /etc/neutron/plugins/ml2/

	if [ ! -f /etc/neutron/plugins/ml2/openvswitch_agent.ini ] ; then
		error "La copie du fichier de configuration 'openvswitch_agent' a échoué" "Copying 'openvswitch_agent' configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying 'openvswitch_agent' configuration file Success"
	fi

	if [ ! -f /etc/neutron/metadata_agent.ini ] ; then
		error "Le fichier de configuration de metadata_agent n'existe pas" "'metadata_agent' configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/neutron/metadata_agent.ini /etc/neutron/metadata_agent.ini.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'metadata_agent.ini.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/metadata_agent.ini /etc/neutron/

	if [ ! -f /etc/neutron/metadata_agent.ini ] ; then
		error "La copie du fichier de configuration a échoué" "Copying configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying configuration file Success"
	fi

	if [ -z $metadata_secret ] ; then
		error "La variable 'METADATA_SECRET' n'est pas définie" "The variable 'METADATA_SECRET' is not defined"
		exit $EXIT_FAILURE
	fi

	sed -ir "s/^[#]*\s*metadata_proxy_shared_secret = .*/metadata_proxy_shared_secret = ${metadata_secret}/" /etc/neutron/metadata_agent.ini

	echo -e "$green[+]$grey Setting 'metadata_agent.ini' Success"

	if [ -f /etc/neutron/metadata_agent.inir ] ; then
		rm -f /etc/neutron/metadata_agent.inir
	fi

	echo -en "\rPopulate the 'neutron' database ..."
	su -s /bin/sh -c "neutron-db-manage --config-file /etc/neutron/neutron.conf --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head" neutron 1> /dev/null 2>&1
	check_populate neutron

	service_restart openvswitch-switch

	ovs-vsctl add-br br-ex
	echo -e "$green[+]$grey Create a new bridge named 'br-ex'"
	ovs-vsctl add-br br-vm
	echo -e "$green[+]$grey Create a new bridge named 'br-vm'"

	service_restart nova-api neutron-server neutron-openvswitch-agent neutron-dhcp-agent neutron-metadata-agent neutron-l3-agent nova-compute

	echo -en "\rCheck each agent started ..."
	sleep 10
	for i in $(openstack network agent list -c "Alive" -c "Agent Type" -f csv | grep -v "Agent Type" | sed 's/ /_/g') ; do
		agent_name=$(echo -e "$i" | cut -d '"' -f 2 | sed 's/_/ /g')
		if [ $(echo -e "$i" | cut -d '"' -f 4) = "False" ] ; then
			error "L'agent '${agent_name}' n'est pas démarré" "'${agent_name}' agent is not started"
			exit $EXIT_FAILURE
		fi
	done
	echo -e "\r$green[+]$grey All agents are started   "

	# Installing the following service
	install_horizon
}
