#!/bin/bash
#coding:utf-8
#Licence: CeCILL-B
#Project: Lab4CE
#Service: Glance

function install_glance(){

	check_point="${FUNCNAME[0]}"

	echo -e "\n[Glance - Image service]"

	check_env_var_defined

	if [ -z $glance_pass ] ; then
		error "La variable 'GLANCE_PASS' n'est pas définie" "The variable 'GLANCE_PASS' is not defined"
		exit $EXIT_FAILURE
	fi
	esc_glance_pass=$(basic_single_escape $glance_pass)

	echo -en "\rCreate the service credentials ..."
	[[ $(openstack user create --domain default --password "${esc_glance_pass}" glance) ]] && echo -en "\rCreate the service credentials [1/3]" || error "Échec lors de la création du service d'identifiants [1/3]" "Failed when creating the identifier service [1/3]"
	openstack role add --project service --user glance admin
	echo -en "\rCreate the service credentials [2/3]"
	[[ $(openstack service create --name glance --description "OpenStack Image" image) ]] && echo -e "\r$green[+]$grey Service credentials Successfully created" || error "Échec lors de la création du service d'identifiants [3/3]" "Failed when creating the identifier service [3/3]"

	echo -en "\rCreate the Image service API endpoints ..."
	[[ $(openstack endpoint create --region RegionOne image public http://controller:9292) ]] && echo -en "\rCreate the Glance service API endpoints [1/3]" || error "Échec lors de la création du point de sortie du service API Glance [1/3]" "Failed when creating the endpoint of the Glance API Service [1/3]"
	[[ $(openstack endpoint create --region RegionOne image internal http://controller:9292) ]] && echo -en "\rCreate the Glance service API endpoints [2/3]" || error "Échec lors de la création du point de sortie du service API Glance [2/3]" "Failed when creating the endpoint of the Glance API Service [2/3]"
	[[ $(openstack endpoint create --region RegionOne image admin http://controller:9292) ]] && echo -e "\r$green[+]$grey Image service API endpoints Successfully created" || error "Échec lors de la création du point de sortie du service API Glance [3/3]" "Failed when creating the endpoint of the Glance API Service [3/3]"

	apt_package glance

	if [ ! -f /etc/glance/glance-api.conf ] ; then
		error "Le fichier de configuration de glance n'existe pas" "Glance configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/glance/glance-api.conf /etc/glance/glance-api.conf.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'glance-api.conf.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/glance-api.conf /etc/glance/

	if [ ! -f /etc/glance/glance-api.conf ] ; then
		error "La copie du fichier de configuration a échoué" "Copying configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying configuration file Success"
	fi

	if [ -z $glance_dbpass ] ; then
		error "La variable 'GLANCE_DBPASS' n'est pas définie" "The variable 'GLANCE_DBPASS' is not defined"
		exit $EXIT_FAILURE
	fi

	sed -ir "s/^[#]*\s*connection = mysql+pymysql.*/connection = mysql+pymysql:\/\/glance:${glance_dbpass}@controller\/glance/" /etc/glance/glance-api.conf
	sed -ir "s/^[#]*\s*password = .*/password = ${glance_pass}/" /etc/glance/glance-api.conf
	echo -e "$green[+]$grey Setting 'glance-api.conf' Success"

	if [ -f /etc/glance/glance-api.confr ] ; then
		rm -f /etc/glance/glance-api.confr
	fi

	echo -en "\rPopulate the 'glance' database ..."
	su -s /bin/sh -c "glance-manage db_sync" glance 1> /dev/null 2>&1
	check_populate glance

	service_restart glance-api

	# Installing the following service
	install_placement
}
