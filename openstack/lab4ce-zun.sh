#!/bin/bash
#coding:utf-8
#Licence: CeCILL-B
#Project: Lab4CE
#Service: Zun

function install_zun(){

	check_point="${FUNCNAME[0]}"

	echo -e "\n[Zun - Containers Service]"

	check_env_var_defined

	if [ -z $zun_pass ] ; then
		error "La variable 'ZUN_PASS' n'est pas définie" "The variable 'ZUN_PASS' is not defined"
		exit $EXIT_FAILURE
	fi
	esc_zun_pass=$(basic_single_escape $zun_pass)

	echo -en "\rCreate the service credentials ..."
	[[ $(openstack user create --domain default --password "${esc_zun_pass}" zun) ]] && echo -en "\rCreate the service credentials [1/3]" || error "Échec lors de la création du service d'identifiants [1/3]" "Failed when creating the identifier service [1/3]"
	openstack role add --project service --user zun admin
	echo -en "\rCreate the service credentials [2/3]"
	[[ $(openstack service create --name zun --description "Container Service" container) ]] && echo -e "\r$green[+]$grey Service credentials Successfully created" || error "Échec lors de la création du service d'identifiants [3/3]" "Failed when creating the identifier service [3/3]"

	echo -en "\rCreate the Zun service API endpoints ..."
	[[ $(openstack endpoint create --region RegionOne container public http://controller:9517/v1) ]] && echo -en "\rCreate the Zun service API endpoints [1/3]" || error "Échec lors de la création du point de sortie du service API Zun [1/3]" "Failed when creating the endpoint of the Zun API Service [1/3]"
	[[ $(openstack endpoint create --region RegionOne container internal http://controller:9517/v1) ]] && echo -en "\rCreate the Zun service API endpoints [2/3]" || error "Échec lors de la création du point de sortie du service API Zun [2/3]" "Failed when creating the endpoint of the Zun API Service [2/3]"
	[[ $(openstack endpoint create --region RegionOne container admin http://controller:9517/v1) ]] && echo -e "\r$green[+]$grey Zun service API endpoints Successfully created" || error "Échec lors de la création du point de sortie du service API Zun [3/3]" "Failed when creating the endpoint of the Zun API Service [3/3]"

	groupadd --system zun 1> /dev/null 2>&1
	if ! getent group zun 1> /dev/null ; then
		error "Le groupe 'zun' n'a pas été créé" "'zun' group has not been created"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey 'zun' system group created"
	fi

	useradd zun --home-dir "/var/lib/zun" --create-home --system --shell /bin/false -g zun 1> /dev/null 2>&1
	if ! getent passwd zun 1> /dev/null ; then
		error "L'utilisateur 'zun' n'a pas été créé" "'zun' user has not been created"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey 'zun' system user created, default home:/var/lib/zun"
	fi

	mkdir -p /etc/zun
	chown zun:zun /etc/zun
	echo -e "$green[+]$grey '/etc/zun' directory created"

	cd /var/lib/zun

	apt_package python3-pip git numactl

	echo -en "\rClone zun repository ..."
	git clone --quiet https://opendev.org/openstack/zun.git
	chown -R zun:zun zun
	echo -e "\r$green[+]$grey zun repository cloned"

	cd zun/

	if [ ! -f requirements.txt ] ; then
		error "Le fichier 'requirements.txt' est introuvable" "File 'requirements.txt' not found"
		exit $EXIT_FAILURE
	fi

	if cat requirements.txt | grep -e "eventlet" 1> /dev/null ; then
		sed -ir "s/^[#]*\s*eventlet.*/eventlet==0.28.0/" requirements.txt
		echo -e "$green[+]$grey Change 'eventlet' package version to 0.28.0 for the proper functioning of Open vSwitch ;)"
	fi

	echo -en "\rInstall zun ..."
	pip3 install -r requirements.txt 1> /dev/null 2>&1
	python3 setup.py install 1> /dev/null 2>&1
	echo -e "\r$green[+]$grey zun installed"

	echo -en "\rGenerate a sample configuration file ..."
	su -s /bin/sh -c "oslo-config-generator --config-file etc/zun/zun-config-generator.conf" zun 1> /dev/null 2>&1
	su -s /bin/sh -c "cp etc/zun/zun.conf.sample /etc/zun/zun.conf" zun 1> /dev/null 2>&1
	echo -e "\r$green[+]$grey Sample configuration file generated  "

	echo -en "\rCopy 'api-paste.ini' ..."
	su -s /bin/sh -c "cp etc/zun/api-paste.ini /etc/zun" zun 1> /dev/null 2>&1
	echo -e "\r$green[+]$grey File 'api-paste.ini' copied Success"

	if [ ! -f /etc/zun/zun.conf ] ; then
		error "Le fichier de configuration de zun n'existe pas" "Zun configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/zun/zun.conf /etc/zun/zun.conf.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'zun.conf.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/zun.conf /etc/zun/

	if [ ! -f /etc/zun/zun.conf ] ; then
		error "La copie du fichier de configuration a échoué" "Copying configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying configuration file Success"
	fi

	if [ -z $controller_ip ] ; then
		error "La variable 'CONTROLLER_IP' n'est pas définie" "The variable 'CONTROLLER_IP' is not defined"
		exit $EXIT_FAILURE
	fi

	if [ -z $zun_dbpass ] ; then
		error "La variable 'ZUN_DBPASS' n'est pas définie" "The variable 'ZUN_DBPASS' is not defined"
		exit $EXIT_FAILURE
	fi

	if [ -z $rabbit_pass ] ; then
		error "La variable 'RABBIT_PASS' n'est pas définie" "The variable 'RABBIT_PASS' is not defined"
		exit $EXIT_FAILURE
	fi

	sed -ir "s/^[#]*\s*transport_url = rabbit:\/\/openstack:.*/transport_url = rabbit:\/\/openstack:${rabbit_pass}@controller/" /etc/zun/zun.conf
	sed -ir "s/^[#]*\s*connection = mysql+pymysql:.*/connection = mysql+pymysql:\/\/zun:${zun_dbpass}@controller\/zun/" /etc/zun/zun.conf

	sed -ir "s/^[#]*\s*host_ip = .*/host_ip = ${controller_ip}/" /etc/zun/zun.conf
	sed -ir "s/^[#]*\s*wsproxy_host = .*/wsproxy_host = ${controller_ip}/" /etc/zun/zun.conf

	# In 2 times in the conf file
	sed -ir "s/^[#]*\s*password = .*/password = ${zun_pass}/" /etc/zun/zun.conf

	echo -e "$green[+]$grey Setting 'zun.conf' Success"

	if [ -f /etc/zun/zun.confr ] ; then
		rm -f /etc/zun/zun.confr
	fi

	echo -en "\rPopulate the 'zun' database ..."
	su -s /bin/sh -c "zun-db-manage upgrade" zun 1> /dev/null 2>&1
	check_populate zun

	if [ ! -f /etc/systemd/system/zun-api.service ] ; then
		cp $CURRENT_PATH/systemd/zun-api.service /etc/systemd/system/
	else
		mv /etc/systemd/system/zun-api.service /etc/systemd/system/zun-api.service.$(date '+%Y-%m-%d')
		echo -e "$green[+]$grey Rename original configuration file to 'zun-api.service.$(date '+%Y-%m-%d')'"

		cp $CURRENT_PATH/systemd/zun-api.service /etc/systemd/system/
	fi

	if [ ! -f /etc/systemd/system/zun-api.service ] ; then
		error "La copie du fichier de configuration a échoué" "Copying upstart config file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying upstart config file Success"
	fi

	if [ ! -f /etc/systemd/system/zun-wsproxy.service ] ; then
		cp $CURRENT_PATH/systemd/zun-wsproxy.service /etc/systemd/system/
	else
		mv /etc/systemd/system/zun-wsproxy.service /etc/systemd/system/zun-wsproxy.service.$(date '+%Y-%m-%d')
		echo -e "$green[+]$grey Rename original configuration file to 'zun-wsproxy.service.$(date '+%Y-%m-%d')'"

		cp $CURRENT_PATH/systemd/zun-wsproxy.service /etc/systemd/system/
	fi

	if [ ! -f /etc/systemd/system/zun-wsproxy.service ] ; then
		error "La copie du fichier de configuration a échoué" "Copying upstart config file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying upstart config file Success"
	fi

	systemctl enable zun-api 1> /dev/null 2>&1
	echo -e "$green[+]$grey Enable 'zun-api' at the start of the computer"

	systemctl enable zun-wsproxy 1> /dev/null 2>&1
	echo -e "$green[+]$grey Enable 'zun-wsproxy' at the start of the computer"

	service_restart zun-api zun-wsproxy

	mkdir -p /etc/cni/net.d
	chown zun:zun /etc/cni/net.d
	echo -e "$green[+]$grey '/etc/cni/net.d' directory created"

	cd /var/lib/zun/zun

	echo -en "\rGenerate a sample configuration file ..."
	su -s /bin/sh -c "cp etc/zun/rootwrap.conf /etc/zun/rootwrap.conf" zun 1> /dev/null 2>&1
	su -s /bin/sh -c "mkdir -p /etc/zun/rootwrap.d" zun 1> /dev/null 2>&1
	su -s /bin/sh -c "cp etc/zun/rootwrap.d/* /etc/zun/rootwrap.d/" zun 1> /dev/null 2>&1
	su -s /bin/sh -c "cp etc/cni/net.d/* /etc/cni/net.d/" zun 1> /dev/null 2>&1
	echo -e "\r$green[+]$grey Sample configuration file generated  "

	echo "zun ALL=(root) NOPASSWD: /usr/local/bin/zun-rootwrap /etc/zun/rootwrap.conf *" | sudo tee /etc/sudoers.d/zun-rootwrap 1> /dev/null 2>&1
	if ! su -s /bin/sh -c "sudo -l" zun | grep -e "(root) NOPASSWD: /usr/local/bin/zun-rootwrap /etc/zun/rootwrap.conf *" 1> /dev/null ; then
		error "Échec de la configuration de sudoers pour l'utilisateur zun" "sudoers configuration for zun users Failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Configure sudoers for zun users Success"
	fi

	mkdir -p /etc/systemd/system/docker.service.d
	echo -e "$green[+]$grey '/etc/systemd/system/docker.service.d' directory created"

	if [ ! -f /etc/systemd/system/docker.service.d/docker.conf ] ; then
		cp $CURRENT_PATH/systemd/docker.conf /etc/systemd/system/docker.service.d/
	else
		mv /etc/systemd/system/docker.service.d/docker.conf /etc/systemd/system/docker.service.d/docker.conf.$(date '+%Y-%m-%d')
		echo -e "$green[+]$grey Rename original configuration file to 'docker.conf.$(date '+%Y-%m-%d')'"

		cp $CURRENT_PATH/systemd/docker.conf /etc/systemd/system/docker.service.d/
	fi

	if [ ! -f /etc/systemd/system/docker.service.d/docker.conf ] ; then
		error "La copie du fichier de configuration a échoué" "Copying upstart config file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying upstart config file Success"
	fi

	systemctl daemon-reload
	echo -e "$green[+]$grey Daemon has been reload"

	service_restart docker

	if [ ! -f /etc/kuryr/kuryr.conf ] ; then
		error "Le fichier de configuration de kuryr n'existe pas, vérifiez l'installation du service Kuryr" "Kuryr configuartion file does not exist, check the Kuryr service installation"
		exit $EXIT_FAILURE
	fi

	sed -ir "s/^[#]*\s*#capability_scope.*/capability_scope = global/" /etc/kuryr/kuryr.conf
	sed -ir "s/^[#]*\s*#process_external_connectivity.*/process_external_connectivity = False/" /etc/kuryr/kuryr.conf

	echo -e "$green[+]$grey Update 'kuryr.conf' Success"

	service_restart kuryr-libnetwork

	containerd config default > /etc/containerd/config.toml

	if [ ! -f /etc/containerd/config.toml ] ; then
		error "Le fichier de configuration du service 'containerd' n'existe pas" "'containerd' service configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	grpc_gid_line=$(cat /etc/containerd/config.toml | grep -e "\[grpc\]" -A 8 -n | grep -e "gid = " | cut -d '-' -f 1)
	zun_group_id=$(getent group zun | cut -d ':' -f 3)

	if [ -z $grpc_gid_line -o -z $zun_group_id ] ; then
		error "Le champs 'gid' ou l'ID du groupe 'zun' est introuvable" "'gid' string or GID 'zun' group not found"
		exit $EXIT_FAILURE
	fi

	sed -ir "${grpc_gid_line}s/^[#]*\s*  gid = .*/  gid = ${zun_group_id}/" /etc/containerd/config.toml

	echo -e "$green[+]$grey Setting 'config.toml' Success"

	if [ -f /etc/containerd/config.tomlr ] ; then
		rm -f /etc/containerd/config.tomlr
	fi

	chown zun:zun /etc/containerd/config.toml
	echo -e "$green[+]$grey Defining default rights for 'config.toml' Success"

	service_restart containerd

	mkdir -p /opt/cni/bin
	echo -e "$green[+]$grey '/opt/cni/bin' directory created"

	cd /opt/cni/bin/

	echo -en "\rDownload the standard loopback plugin ..."
	curl -L https://github.com/containernetworking/plugins/releases/download/v0.7.1/cni-plugins-amd64-v0.7.1.tgz -O . 1> /dev/null 2>&1
	tar zxvf /opt/cni/bin/cni-plugins-amd64-v0.7.1.tgz 1> /dev/null
	find . ! -name loopback -delete
	echo -e "\r$green[+]$grey Download the standard loopback plugin Success"

	echo -en "\rInstall the Zun CNI plugin ..."
	install -o zun -m 0555 -D /usr/local/bin/zun-cni /opt/cni/bin/zun-cni
	echo -e "\r$green[+]$grey Zun CNI plugin successfully installed"

	if [ ! -f /etc/systemd/system/zun-compute.service ] ; then
		cp $CURRENT_PATH/systemd/zun-compute.service /etc/systemd/system/
	else
		mv /etc/systemd/system/zun-compute.service /etc/systemd/system/zun-compute.service.$(date '+%Y-%m-%d')
		echo -e "$green[+]$grey Rename original configuration file to 'zun-compute.service.$(date '+%Y-%m-%d')'"

		cp $CURRENT_PATH/systemd/zun-compute.service /etc/systemd/system/
	fi

	if [ ! -f /etc/systemd/system/zun-compute.service ] ; then
		error "La copie du fichier de configuration pour zun compute a échoué" "Copying upstart config file for zun compute failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying upstart config file for zun compute Success"
	fi

	if [ ! -f /etc/systemd/system/zun-cni-daemon.service ] ; then
		cp $CURRENT_PATH/systemd/zun-cni-daemon.service /etc/systemd/system/
	else
		mv /etc/systemd/system/zun-cni-daemon.service /etc/systemd/system/zun-cni-daemon.service.$(date '+%Y-%m-%d')
		echo -e "$green[+]$grey Rename original configuration file to 'zun-cni-daemon.service.$(date '+%Y-%m-%d')'"

		cp $CURRENT_PATH/systemd/zun-cni-daemon.service /etc/systemd/system/
	fi

	if [ ! -f /etc/systemd/system/zun-cni-daemon.service ] ; then
		error "La copie du fichier de configuration pour le daemon zun cni a échoué" "Copying upstart config file for zun cni daemon failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying upstart config file for zun cni daemon Success"
	fi

	systemctl enable zun-compute 1> /dev/null 2>&1
	echo -e "$green[+]$grey Enable 'zun-compute' at the start of the computer"

	systemctl enable zun-cni-daemon 1> /dev/null 2>&1
	echo -e "$green[+]$grey Enable 'zun-cni-daemon' at the start of the computer"

	service_restart docker
	sleep 5
	service_restart zun-compute zun-cni-daemon

	pip3_package python-zunclient

	echo -en "\rCheck Host started ..."
	sleep 10
	for i in $(openstack appcontainer service list -c "Host" -c "Disabled" -f csv | grep -v "Disabled" | sed 's/ /_/g') ; do
		compute_name=$(echo -e "$i" | cut -d '"' -f 2 | sed 's/_/ /g')
		if [ $(echo -e "$i" | cut -d ',' -f 2) = "False" ] ; then
			echo -e "\r$green[+]$grey Host is enable     "
		else
			if [ -z "$compute_name" ] ; then
				error "L'hôte '${compute_name}' est introuvable" "'${compute_name}' was not found"
			else
				error "L'hôte '${compute_name}' n'est pas démarré" "'${compute_name}' host is not started"
			fi
			exit $EXIT_FAILURE
		fi
	done

	# Installing the following sequence
	install_image
}
