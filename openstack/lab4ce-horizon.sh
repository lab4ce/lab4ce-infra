#!/bin/bash
#coding:utf-8
#Licence: CeCILL-B
#Project: Lab4CE
#Service: Horizon

function install_horizon(){

	check_point="${FUNCNAME[0]}"

	echo -e "\n[Horizon - Dashboard]"

	check_env_var_defined

	# For a database session back -> https://docs.openstack.org/horizon/victoria/admin/sessions.html

	apt_package openstack-dashboard

	echo -e "$white[i]$grey Using Memcached for session back"

	if [ ! -f /etc/openstack-dashboard/local_settings.py ] ; then
		error "Le fichier de configuration de horizon n'existe pas" "Horizon configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/openstack-dashboard/local_settings.py /etc/openstack-dashboard/local_settings.py.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'local_settings.py.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/local_settings.py /etc/openstack-dashboard/

	if [ ! -f /etc/openstack-dashboard/local_settings.py ] ; then
		error "La copie du fichier de configuration a échoué" "Copying configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying configuration file Success"
	fi

	if [ ! -f /etc/apache2/conf-available/openstack-dashboard.conf ] ; then
		error "Le fichier de configuration de horizon n'existe pas" "Horizon configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	if ! cat /etc/apache2/conf-available/openstack-dashboard.conf | grep -e "WSGIApplicationGroup %{GLOBAL}" 1> /dev/null ; then
		echo -e "WSGIApplicationGroup %{GLOBAL}" >> /etc/apache2/conf-available/openstack-dashboard.conf
		echo -e "$green[+]$grey WSGIApplicationGroup added inside 'openstack-dashboard.conf'"
	else
		echo -e "$green[+]$grey WSGIApplicationGroup already exist inside 'openstack-dashboard.conf'"
	fi

	service_restart apache2 memcached

	# Installing the following service
	install_cinder
}
