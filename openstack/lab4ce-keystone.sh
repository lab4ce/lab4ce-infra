#!/bin/bash
#coding:utf-8
#Licence: CeCILL-B
#Project: Lab4CE
#Service: Keystone

function install_keystone(){

	check_point="${FUNCNAME[0]}"

	echo -e "\n[Keystone - Identity Service]"

	apt_package keystone

	if [ ! -f /etc/keystone/keystone.conf ] ; then
		error "Le fichier de configuration de keystone n'existe pas" "Keystone configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/keystone/keystone.conf /etc/keystone/keystone.conf.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'keystone.conf.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/keystone.conf /etc/keystone/

	if [ ! -f /etc/keystone/keystone.conf ] ; then
		error "La copie du fichier de configuration a échoué" "Copying configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying configuration file Success"
	fi

	if [ -z $keystone_dbpass ] ; then
		error "La variable 'KEYSTONE_DBPASS' n'est pas définie" "The variable 'KEYSTONE_DBPASS' is not defined"
		exit $EXIT_FAILURE
	fi

	sed -ir "s/^[#]*\s*connection = .*/connection = mysql+pymysql:\/\/keystone:${keystone_dbpass}@controller\/keystone/" /etc/keystone/keystone.conf
	echo -e "$green[+]$grey Setting 'keystone.conf' Success"

	if [ -f /etc/keystone/keystone.confr ] ; then
		rm -f /etc/keystone/keystone.confr
	fi

	echo -en "\rPopulate the 'keystone' database ..."
	su -s /bin/sh -c "keystone-manage db_sync" keystone 1> /dev/null 2>&1
	check_populate keystone
	
	echo -en "\rInitialize Fernet key repositories ..."
	keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone
	echo -en "\rInitialize Fernet key repositories [1/2]"
	keystone-manage credential_setup --keystone-user keystone --keystone-group keystone
	echo -e "\r$green[+]$grey Initialize Fernet key repositories Success"

	echo -en "\rBootstrap the Identity service ..."
	if [ -z $admin_pass ] ; then
		error "La variable 'ADMIN_PASS' n'est pas définie" "The variable 'ADMIN_PASS' is not defined"
		exit $EXIT_FAILURE
	fi
	esc_admin_pass=$(basic_single_escape $admin_pass)

	keystone-manage bootstrap --bootstrap-password "$esc_admin_pass" \
	  --bootstrap-admin-url http://controller:5000/v3/ \
	  --bootstrap-internal-url http://controller:5000/v3/ \
	  --bootstrap-public-url http://controller:5000/v3/ \
	  --bootstrap-region-id RegionOne
	echo -e "\r$green[+]$grey Bootstrap the Identity service, ADMIN_PASS defined"

	if ! which apache2 1> /dev/null ; then
		apt_package apache2
	fi

	if [ ! -f /etc/apache2/apache2.conf ] ; then
		error "Le fichier de configuration d'Apache est introuvable" "Apache2 configuration file not found"
		exit $EXIT_FAILURE
	fi

	if ! cat /etc/apache2/apache2.conf | grep -e "ServerName controller" 1> /dev/null ; then
		echo -e "ServerName controller" >> /etc/apache2/apache2.conf
		echo -e "$green[+]$grey Apache HTTP server configuration defined"
	else
		echo -e "$green[+]$grey Apache HTTP server configuration already defined"
	fi

	service_restart apache2

	echo -e "export OS_PROJECT_DOMAIN_NAME=Default" > ~/admin-openrc
	echo -e "export OS_USER_DOMAIN_NAME=Default" >> ~/admin-openrc
	echo -e "export OS_PROJECT_NAME=admin" >> ~/admin-openrc
	echo -e "export OS_USERNAME=admin" >> ~/admin-openrc
	echo -e "export OS_PASSWORD=${esc_admin_pass}" >> ~/admin-openrc
	echo -e "export OS_AUTH_URL=http://controller:5000/v3" >> ~/admin-openrc
	echo -e "export OS_IDENTITY_API_VERSION=3" >> ~/admin-openrc
	echo -e "export OS_IMAGE_API_VERSION=2" >> ~/admin-openrc

	echo -e "\necho -e \"> \$OS_PROJECT_NAME\"" >> ~/admin-openrc
	echo -e "echo -e \"> \$OS_USERNAME\"" >> ~/admin-openrc

	echo -e "$green[+]$grey '~/admin-openrc' script create Success"

	. ~/admin-openrc 1> /dev/null

	# Check if the fisrt env varaible is empty
	if [ -z $OS_PROJECT_DOMAIN_NAME ] ; then
		error "Les variables d'environnement d'OpenStack ne sont pas définies" "OpenStack global environment variables not correctly defined"

		export OS_PROJECT_DOMAIN_NAME=Default
		export OS_USER_DOMAIN_NAME=Default
		export OS_PROJECT_NAME=admin
		export OS_USERNAME=admin
		export OS_PASSWORD=${esc_admin_pass}
		export OS_AUTH_URL=http://controller:5000/v3
		export OS_IDENTITY_API_VERSION=3
		export OS_IMAGE_API_VERSION=2
		echo -e "$yellow[!]$grey OpenStack global environment variables manually defined"
	else
		echo -e "$green[+]$grey Export OpenStack global variables"
	fi

	echo -en "\rCreate the 'service' project ..."
	[[ $(openstack project create --domain default --description "Service Project" service) ]] && echo -e "\r$green[+]$grey 'service' project Successfully created" || error "Échec lors de la création du projet 'service'" "Failed when creating the 'service' project"

	echo -en "\rTry to request an authentication token ..."
	if ! openstack token issue 1> /dev/null 2>&1 ; then
		error "La demande d'un token d'authentifiation a échoué" "The request for an authentication token failed"
		exit $EXIT_FAILURE
	else
		echo -e "\r$green[+]$grey Try to request an authentication token Success"
	fi

	# Installing the following service
	install_glance
}
