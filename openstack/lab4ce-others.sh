#!/bin/bash
#coding:utf-8
#Licence: CeCILL-B
#Project: Lab4CE

function install_end(){
	check_point="${FUNCNAME[0]}"

	echo -e """\n$white
\t                 __   
\t                /\ \    
\t   __    ___    \_\ \   
\t /'__\`\/' _ \`\  /'_\` \  
\t/\  __//\ \/\ \/\ \L\ \ 
\t\ \____\ \_\ \_\ \___,_\\
\t \/____/\/_/\/_/\/__,_ /$grey\n\n"""

	echo -e "Horizon dashboard URL access: http://controller/horizon"

	echo -e "\nOpenStack ${white}Users${grey}"
	for user in $(openstack user list -f csv -c "Name" | grep -E "student|teacher|admin" | cut -d '"' -f 2) ; do
		if [ "$user" = "admin" ] ; then
			echo -e "- ${user} (default)"
		else
			echo -e "- ${user}"
		fi
	done

	echo -e "\nOpenStack ${white}Projects${grey}"
	for project in $(openstack project list -f csv -c "Name" | grep -v "service\|Name" | cut -d '"' -f 2) ; do
		if [ "$project" = "admin" ] ; then
			echo -e "- ${project} (default)"
		else
			echo -e "- ${project}"
		fi
	done

	echo -e "\nOpenStack ${white}Subnet${grey}"
	for subnet in $(openstack subnet list -c "Name" -c "Subnet" -f csv | grep -v '"Name",' | sed 's/ /_/g') ; do
		subnet_name=$(echo -e "$subnet" | cut -d '"' -f 2 | sed 's/_/ /g')
		subnet_network=$(echo -e "$subnet" | cut -d '"' -f 4 | sed 's/_/ /g')

		rows="%-28s %-25s\n"
		printf "$rows" "- ${subnet_name}" "(${subnet_network})"
	done

	echo -e "\nOther tools have been implemented like:"
	echo -e "\t- ${white}check_script${grey} : See OpenStack configuration scripts"
	echo -e "\t- ${white}check_logs${grey} : logs OpenStack files management"
	echo -e "\t- ${white}check_services${grey} : Check active services"

	cd $CURRENT_PATH
  	nb_cpus=$(. $CURRENT_PATH/${0##*/} --cpu)
  	echo -e "\nCurrent CPU cores: $nb_cpus"

  	if [ $nb_cpus -le 2 ] ; then
  		echo -e "$white[i]$grey For better operation, increase the number of CPUs"
  	fi

  	if which check_services 1> /dev/null ; then
  		echo
  		check_services
  	fi

	echo -e "\n$white[i]$grey It is recommended to restart the machine to finalize installation"
	read -n 1 -p "[?] Confirm restart now (Y/n) " restart

	if [[ $restart =~ ^[YyOo]$ ]] ; then
		sudo reboot
	fi

	exit $EXIT_SUCCESS
}

function install_useful_tools(){
	check_point="${FUNCNAME[0]}"

	echo -e "\n[Add useful tools]"

	if ! cat ~/.bashrc | grep -e "alias op='openstack'" 1> /dev/null ; then
		echo -e "alias op='openstack'" >> ~/.bashrc
		echo -e "$green[+]$grey Create OpenStack alias in ~/.bashrc, use now 'op' Success"
	fi

	if [ ! -f /usr/local/sbin/check_script ] ; then
		cp $CURRENT_PATH/tools/check_script.sh /usr/local/sbin/check_script
		chmod +x /usr/local/sbin/check_script
		echo -e "$green[+]$grey New local tool 'check_script' installed"
	fi

	if [ ! -f /usr/local/sbin/check_logs ] ; then
		cp $CURRENT_PATH/tools/check_logs.sh /usr/local/sbin/check_logs
		chmod +x /usr/local/sbin/check_logs
		echo -e "$green[+]$grey New local tool 'check_logs' installed"
	fi

	if [ ! -f /usr/local/sbin/check_services ] ; then
		cp $CURRENT_PATH/tools/check_services.sh /usr/local/sbin/check_services
		chmod +x /usr/local/sbin/check_services
		echo -e "$green[+]$grey New local tool 'check_services' installed"
	fi

	install_end
}

function install_nova_policy(){
	check_point="${FUNCNAME[0]}"

	echo -e "\n[Add Nova Policy]"

	if [ ! -f /etc/nova/policy.yaml ] ; then
		cp $CURRENT_PATH/policy/nova-policy.yaml /etc/nova/policy.yaml
	else
		mv /etc/nova/policy.yaml /etc/nova/policy.yaml.$(date '+%Y-%m-%d')
		echo -e "$green[+]$grey Rename original configuration file to 'policy.yaml.$(date '+%Y-%m-%d')'"

		cp $CURRENT_PATH/policy/nova-policy.yaml /etc/nova/policy.yaml
	fi

	if [ ! -f /etc/nova/policy.yaml ] ; then
		error "La copie du fichier de stratégie 'policy.yaml' a échoué" "Copying 'policy.yaml' policy file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying 'policy.yaml' configuration file Success"
	fi

	install_useful_tools
}

function install_keystone_policy(){
	check_point="${FUNCNAME[0]}"

	echo -e "\n[Add Keystone Policy]"

	echo -e "$white[i]$grey Aucune stratégie définie pour les projets, par défaut seul l'admin peut les créer"

	install_nova_policy
}

function install_role_policy(){
	check_point="${FUNCNAME[0]}"

	echo -e "\n[Config the role policy]"

	# Création d'un environnement de test

	test_openstack_passwd="azerty"
	echo -en "\rCreate the OpenStack users ..."
	[[ $(openstack user create --domain default --password "${test_openstack_passwd}" teacher_1) ]] && echo -en "\rCreate 'teacher_1' OpenStack user [1/5]" || error "Échec lors de la création de l'utilisateur [1/5]" "Failed when creating user [1/5]"
	[[ $(openstack user create --domain default --password "${test_openstack_passwd}" teacher_2) ]] && echo -en "\rCreate 'teacher_2' OpenStack user [2/5]" || error "Échec lors de la création de l'utilisateur [2/5]" "Failed when creating user [2/5]"
	[[ $(openstack user create --domain default --password "${test_openstack_passwd}" student_1) ]] && echo -en "\rCreate 'student_1' OpenStack user [3/5]" || error "Échec lors de la création de l'utilisateur [3/5]" "Failed when creating user [3/5]"
	[[ $(openstack user create --domain default --password "${test_openstack_passwd}" student_2) ]] && echo -en "\rCreate 'student_2' OpenStack user [4/5]" || error "Échec lors de la création de l'utilisateur [4/5]" "Failed when creating user [4/5]"
	[[ $(openstack user create --domain default --password "${test_openstack_passwd}" student_3) ]] && echo -e "\r$green[+]$grey OpenStack users Successfully created" || error "Échec lors de la création de l'utilisateur [5/5]" "Failed when creating user [5/5]"

	echo -en "\rCreate the OpenStack groups ..."
	[[ $(openstack group create teacher) ]] && echo -en "\rCreate 'teacher' OpenStack group [1/2]" || error "Échec lors de la création du groupe [1/2]" "Failed when creating group [1/2]"
	[[ $(openstack group create student) ]] && echo -e "\r$green[+]$grey OpenStack groups Successfully created" || error "Échec lors de la création du groupe [2/2]" "Failed when creating group [2/2]"

	echo -en "\rAdd users in groups ..."
	openstack group add user teacher teacher_1
	echo -en "\rAdd 'teacher_1' user in 'teacher' group [1/5]"
	openstack group add user teacher teacher_2
	echo -en "\rAdd 'teacher_2' user in 'teacher' group [2/5]"
	openstack group add user student student_1
	echo -en "\rAdd 'student_1' user in 'student' group [3/5]"
	openstack group add user student student_2
	echo -en "\rAdd 'student_2' user in 'student' group [4/5]"
	openstack group add user student student_3
	echo -e "\r$green[+]$grey Add users in groups Success              "

	echo -en "\rCreate the OpenStack roles ..."
	[[ $(openstack role create teacher) ]] && echo -en "\rCreate 'teacher' role [1/4]" || error "Échec lors de la création du rôle [1/4]" "Failed when creating role [1/4]"
	[[ $(openstack role create student) ]] && echo -en "\rCreate 'student' role [2/4]" || error "Échec lors de la création du rôle [2/4]" "Failed when creating role [2/4]"
	[[ $(openstack role create guest) ]] && echo -en "\rCreate 'guest' role [3/4]" || error "Échec lors de la création du rôle [3/4]" "Failed when creating role [3/4]"
	[[ $(openstack role create owner) ]] && echo -e "\r$green[+]$grey OpenStack roles Successfully created" || error "Échec lors de la création du rôle [4/4]" "Failed when creating role [4/4]"

	echo -en "\rCreate an OpenStack project ..."
	[[ $(openstack project create TPShell --domain Default) ]] && echo -e "\r$green[+]$grey OpenStack project Successfully created" || error "Échec lors de la création du projet" "Failed when creating project"

	echo -en "\rAdd role for groups and project ..."
	openstack role add teacher --project TPShell --group teacher --inherited
	echo -en "\rAdd 'teacher' role [1/2]"
	openstack role add student --project TPShell --group student
	echo -e "\r$green[+]$grey Add roles for groups and project Success"

	echo -en "\rAdd role for users and project ... (Nova Policy Problem)"
	openstack role add teacher --project TPShell --user teacher_1
	echo -en "\rAdd 'teacher' role [1/5]                                "
	openstack role add teacher --project TPShell --user teacher_2
	echo -en "\rAdd 'teacher' role [2/5]                                "
	openstack role add student --project TPShell --user student_1
	echo -en "\rAdd 'student' role [3/5]                                "
	openstack role add student --project TPShell --user student_2
	echo -en "\rAdd 'student' role [4/5]                                "
	openstack role add student --project TPShell --user student_3
	echo -e "\r$green[+]$grey Add roles for users and project Success             "

	echo -en "\rCreate an OpenStack sub project ..."
	[[ $(openstack project create Lab1 --parent TPShell --domain Default) ]] && echo -e "\r$green[+]$grey OpenStack sub project Successfully created" || error "Échec lors de la création du sous projet" "Failed when creating sub project"

	echo -en "\rAdd owner role for the proprietary of the sub project ..."
	openstack role add owner --project Lab1 --user student_1
	echo -e "\r$green[+]$grey Add owner role for the proprietary of the sub project Success"

	echo -en "\rAdd guest role for the sub project ..."
	openstack role add guest --project Lab1 --user student_2
	echo -e "\r$green[+]$grey Add guest role for the sub project Success"

	echo -e "export OS_PROJECT_DOMAIN_NAME=Default" > ~/user-openrc
	echo -e "export OS_USER_DOMAIN_NAME=Default" >> ~/user-openrc
	echo -e "export OS_PASSWORD=azerty" >> ~/user-openrc
	echo -e "export OS_AUTH_URL=http://controller:5000/v3" >> ~/user-openrc
	echo -e "export OS_IDENTITY_API_VERSION=3" >> ~/user-openrc
	echo -e "export OS_IMAGE_API_VERSION=2" >> ~/user-openrc

	echo -e "\n#==(projects)" >> ~/user-openrc
	echo -e "export OS_PROJECT_NAME=TPShell" >> ~/user-openrc
	echo -e "#export OS_PROJECT_NAME=Lab1" >> ~/user-openrc

	echo -e "\n#==(users)" >> ~/user-openrc
	echo -e "export OS_USERNAME=teacher_1" >> ~/user-openrc
	echo -e "#export OS_USERNAME=teacher_2" >> ~/user-openrc
	echo -e "#export OS_USERNAME=student_1" >> ~/user-openrc
	echo -e "#export OS_USERNAME=student_2" >> ~/user-openrc
	echo -e "#export OS_USERNAME=student_3" >> ~/user-openrc

	echo -e "\necho -e \"> \$OS_PROJECT_NAME\"" >> ~/user-openrc
	echo -e "echo -e \"> \$OS_USERNAME\"" >> ~/user-openrc

	echo -e "$green[+]$grey '~/user-openrc' script create Success"

	install_keystone_policy
}

function install_neutron_network(){
	check_point="${FUNCNAME[0]}"

	check_env_var_defined

	echo -e "\n[Config Lab4CE network]"

	echo -en "\rCreate the network 'external-net' ..."
	[[ $(neutron net-create --router:external --provider:physical_network externalnet --provider:network_type flat external-net) ]] && echo -e "\r$green[+]$grey 'external-net' network Successfully created" || error "Échec lors de la création du réseau 'external-net'" "Failed when creating the 'external-net' network"

	echo -en "\rCreate the network 'common-net' ..."
	[[ $(neutron net-create --shared common-net) ]] && echo -e "\r$green[+]$grey 'common-net' network Successfully created" || error "Échec lors de la création du réseau 'common-net'" "Failed when creating the 'common-net' network"

	echo -en "\rCreate the subnet 'external-subnet' ..."
	[[ $(neutron subnet-create --name external-subnet --gateway 10.1.0.1 --allocation-pool start=10.1.0.2,end=10.1.0.254 --enable-dhcp external-net 10.1.0.0/24) ]] && echo -e "\r$green[+]$grey 'external-subnet' subnet on '10.1.0.0/24' Successfully created" || error "Échec lors de la création du sous réseau 'external-subnet'" "Failed when creating the 'external-subnet' subnet"

	echo -en "\rCreate the subnet 'common-subnet' ..."
	[[ $(neutron subnet-create --name common-subnet --gateway 10.1.1.1 --allocation-pool start=10.1.1.2,end=10.1.1.254 --dns-nameserver 8.8.8.8 --enable-dhcp common-net 10.1.1.0/24) ]] && echo -e "\r$green[+]$grey 'common-subnet' subnet on '10.1.1.0/24' Successfully created" || error "Échec lors de la création du sous réseau 'common-subnet'" "Failed when creating the 'common-subnet' subnet"

	echo -en "\rCreate the router 'common-router' ..."
	[[ $(neutron router-create --distributed False --ha False common-router) ]] && echo -e "\r$green[+]$grey 'common-router' router Successfully created" || error "Échec lors de la création du routeur 'common-router'" "Failed when creating the 'common-router' router"

	echo -en "\rSet the external gateway ..."
	[[ $(neutron router-gateway-set common-router external-net) ]] && echo -e "\r$green[+]$grey External gateway Successfully created" || error "Échec lors de la création de la passerelle externe" "Failed when creating the external gateway"

	echo -en "\rAttaching the subnet to the router ..."
	[[ $(neutron router-interface-add common-router common-subnet) ]] && echo -e "\r$green[+]$grey Attaching the subnet to the router Successfully created" || error "Échec lors de liaison du sous réseau au router" "Failed to attaching the subnet to the router"

	install_role_policy
}

function install_vm_flavor(){
	check_point="${FUNCNAME[0]}"

	check_env_var_defined

	echo -e "\n[Create VM Flavor]"

	echo -en "\rCreate OpenStack VM flavors ..."
	[[ $(openstack flavor create --id 0 --ram 256 --disk 1 --vcpus 1 --public lab4ce.tiny) ]] && echo -en "\rCreate flavor 'lab4ce.tiny' [1/2]" || error "Échec lors de la création du gabarit [1/2]" "Failed when creating the flavor [1/2]"
	[[ $(openstack flavor create --id 1 --ram 512 --disk 2 --vcpus 1 --public lab4ce.small) ]] && echo -e "\r$green[+]$grey Flavors Successfully created" || error "Échec lors de la création du gabarit [2/2]" "Failed when creating the flavor [2/2]"

	install_neutron_network
}

function install_image(){
	check_point="${FUNCNAME[0]}"

	check_env_var_defined

	echo -e "\n[Add Cirros Image]"
	#https://docs.openstack.org/glance/victoria/install/verify.html

	echo -en "\rCheck Cirros image exist ..."
	sleep 5
	for i in $(openstack image list -c "Name" -f csv | grep -v "Name" | sed 's/ /\\/g') ; do
		image_name=$(echo -e "$i" | cut -d '"' -f 2 | sed 's/\\/ /g')
		if [ "${image_name}" = "cirros-0.4.0-x86_64" ] ; then
			echo -e "\r$green[+]$grey Cirros image already exist       "
			state_image_created=1
		fi
	done

	if [ "$state_image_created" != "1" ] ; then
		echo -en "\rDownload Cirros image ...   "
		wget -P /tmp/images http://download.cirros-cloud.net/0.4.0/cirros-0.4.0-x86_64-disk.img 1> /dev/null 2>&1
		if [ ! -f /tmp/images/cirros-0.4.0-x86_64-disk.img ] ; then
			error "Échec du télécharment, fichier introuvable" "Download failed, file not found"
			exit $EXIT_FAILURE
		fi
		echo -e "\r$green[+]$grey Cirros image download Success"

		echo -en "\rAdd image in OpenStack ..."
		[[ $(openstack image create "cirros-0.4.0-x86_64" --file /tmp/images/cirros-0.4.0-x86_64-disk.img --disk-format qcow2 --container-format bare --public) ]] && echo -e "\r$green[+]$grey Add image in OpenStack Success" || error "Échec lors de la création de l'image" "Failed when creating image"

		echo -en "\rCheck Cirros image created ..."
		sleep 5
		for i in $(openstack image list -c "Name" -f csv | grep -v "Name" | sed 's/ /\\/g') ; do
			image_name=$(echo -e "$i" | cut -d '"' -f 2 | sed 's/\\/ /g')
			if [ "${image_name}" = "cirros-0.4.0-x86_64" ] ; then
				echo -e "\r$green[+]$grey Cirros image has been Successfully created"
				state_image_created=1
			fi
		done

		if [ "$state_image_created" != "1" ] ; then
			error "L'image '${image_name}' n'a pas été créée" "'${image_name}' image has not been created"
			exit $EXIT_FAILURE
		fi
	fi

	install_vm_flavor
}
