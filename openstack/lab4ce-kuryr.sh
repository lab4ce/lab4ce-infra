#!/bin/bash
#coding:utf-8
#Licence: CeCILL-B
#Project: Lab4CE
#Service: Kuryr

function install_kuryr(){

	check_point="${FUNCNAME[0]}"

	echo -e "\n[Kuryr - Networking integration for containers]"

	check_env_var_defined

	apt_package apt-transport-https ca-certificates software-properties-common

	if ! which docker 1> /dev/null ; then
		curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - 1> /dev/null 2>&1
		echo -e "$green[+]$grey The GPG key for the official Docker repository added"
		echo -en "\rAdd the Docker repository to APT sources ..."
		add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable" 1> /dev/null 2>&1
		echo -e "\r$green[+]$grey Docker repository Added to APT sources      "
		apt_package docker-ce
	else
		echo -e "$green[+]$grey Docker already installed"
	fi

	service_restart docker

	if [ -z $kuryr_pass ] ; then
		error "La variable 'KURYR_PASS' n'est pas définie" "The variable 'KURYR_PASS' is not defined"
		exit $EXIT_FAILURE
	fi
	esc_kuryr_pass=$(basic_single_escape $kuryr_pass)

	echo -en "\rCreate the service credentials ..."
	[[ $(openstack user create --domain default --password "${esc_kuryr_pass}" kuryr) ]] && echo -en "\rCreate the service credentials [1/2]" || error "Échec lors de la création du service d'identifiants [1/2]" "Failed when creating the identifier service [1/2]"
	openstack role add --project service --user kuryr admin
	echo -e "\r$green[+]$grey Service credentials Successfully created"

	groupadd --system kuryr 1> /dev/null 2>&1
	if ! getent group kuryr 1> /dev/null ; then
		error "Le groupe 'kuryr' n'a pas été créé" "'kuryr' group has not been created"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey 'kuryr' system group created"
	fi

	useradd kuryr --create-home --home-dir "/var/lib/kuryr" --system --shell /bin/false --gid kuryr 1> /dev/null 2>&1
	if ! getent passwd kuryr 1> /dev/null ; then
		error "Le groupe 'kuryr' n'a pas été créé" "'kuryr' group has not been created"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey 'kuryr' system user created, default home:/var/lib/kuryr"
	fi

	mkdir -p /etc/kuryr
	chown kuryr:kuryr /etc/kuryr
	echo -e "$green[+]$grey '/etc/kuryr' directory created"

	cd /var/lib/kuryr

	echo -en "\rClone kuryr-libnetwork repository..."
	git clone -b master --quiet https://opendev.org/openstack/kuryr-libnetwork.git
	chown -R kuryr:kuryr kuryr-libnetwork
	echo -e "\r$green[+]$grey kuryr-libnetwork repository cloned"

	cd kuryr-libnetwork/

	echo -en "\rInstall kuryr-libnetwork ..."
	pip3 install -r requirements.txt 1> /dev/null 2>&1
	python3 setup.py install 1> /dev/null 2>&1
	echo -e "\r$green[+]$grey kuryr-libnetwork installed"

	echo -en "\rGenerate a sample configuration file ..."
	su -s /bin/sh -c "./tools/generate_config_file_samples.sh" kuryr 1> /dev/null 2>&1
	su -s /bin/sh -c "cp etc/kuryr.conf.sample /etc/kuryr/kuryr.conf" kuryr 1> /dev/null 2>&1
	echo -e "\r$green[+]$grey Sample configuration file generated  "

	if [ ! -f /etc/kuryr/kuryr.conf ] ; then
		error "Le fichier de configuration de kuryr n'existe pas" "Kuryr configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/kuryr/kuryr.conf /etc/kuryr/kuryr.conf.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'kuryr.conf.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/kuryr.conf /etc/kuryr/

	if [ ! -f /etc/kuryr/kuryr.conf ] ; then
		error "La copie du fichier de configuration a échoué" "Copying configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying configuration file Success"
	fi

	sed -ir "s/^[#]*\s*password = KURYR_PASS.*/password = ${kuryr_pass}/" /etc/kuryr/kuryr.conf

	echo -e "$green[+]$grey Setting 'kuryr.conf' Success"

	if [ -f /etc/kuryr/kuryr.confr ] ; then
		rm -f /etc/kuryr/kuryr.confr
	fi

	chown -R kuryr:kuryr /etc/kuryr
	echo -e "$green[+]$grey '/etc/kuryr' directory created"

	if [ ! -f /etc/systemd/system/kuryr-libnetwork.service ] ; then
		cp $CURRENT_PATH/systemd/kuryr-libnetwork.service /etc/systemd/system/
	else
		mv /etc/systemd/system/kuryr-libnetwork.service /etc/systemd/system/kuryr-libnetwork.service.$(date '+%Y-%m-%d')
		echo -e "$green[+]$grey Rename original configuration file to 'kuryr-libnetwork.service.$(date '+%Y-%m-%d')'"

		cp $CURRENT_PATH/systemd/kuryr-libnetwork.service /etc/systemd/system/
	fi

	if [ ! -f /etc/systemd/system/kuryr-libnetwork.service ] ; then
		error "La copie du fichier de configuration a échoué" "Copying upstart config file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying upstart config file Success"
	fi

	systemctl enable kuryr-libnetwork 1> /dev/null 2>&1
	echo -e "$green[+]$grey Enable 'kuryr-libnetwork' at the start of the computer"

	service_restart kuryr-libnetwork docker

	echo -en "\rCreation of the Docker network 'test_net' on '10.10.0.0/16' ..."
	docker network create --driver kuryr --ipam-driver kuryr --subnet 10.10.0.0/16 --gateway=10.10.0.1 test_net 1> /dev/null 2>&1
	if ! docker network list | grep -e "test_net" 1> /dev/null ; then
		error "La création du réseau Docker 'test_net' avec le driver 'kuryr' a échoué" "The creation of the Docker network 'test_net' with the 'kuryr' driver failed"
		exit $EXIT_FAILURE
	else
		echo -e "\r$green[+]$grey Creation of the Docker network 'test_net' with the 'kuryr' driver on the subnet '10.10.0.0/16' Success"
	fi

	echo -en "\rStarted a test Docker container with the network 'test_net' ..."
	if ! docker run --net test_net cirros ifconfig 1> /dev/null 2>&1 ; then
		error "Échec du lancement du conteneur Docker de test" "Failed to launch the test Docker container"
		exit $EXIT_FAILURE
	else
		echo -e "\r$green[+]$grey The test Docker container has successfully worked           "
	fi

	# Installing the following service
	install_zun
}
