#!/bin/bash
#coding:utf-8
#Licence: CeCILL-B
#Project: Lab4CE
#Service: Cinder

function install_cinder(){

	check_point="${FUNCNAME[0]}"

	echo -e "\n[Cinder - Block Storage]"

	check_env_var_defined

	if [ -z $cinder_pass ] ; then
		error "La variable 'CINDER_PASS' n'est pas définie" "The variable 'CINDER_PASS' is not defined"
		exit $EXIT_FAILURE
	fi
	esc_cinder_pass=$(basic_single_escape $cinder_pass)

	echo -en "\rCreate the service credentials ..."
	[[ $(openstack user create --domain default --password "${esc_cinder_pass}" cinder) ]] && echo -en "\rCreate the service credentials [1/4]" || error "Échec lors de la création du service d'identifiants [1/4]" "Failed when creating the identifier service [1/4]"
	openstack role add --project service --user cinder admin
	echo -en "\rCreate the service credentials [2/4]"
	[[ $(openstack service create --name cinderv2 --description "OpenStack Block Storage" volumev2) ]] && echo -en "\rCreate the service credentials [3/4]" || error "Échec lors de la création du service d'identifiants [3/4]" "Failed when creating the identifier service [3/4]"
	[[ $(openstack service create --name cinderv3 --description "OpenStack Block Storage" volumev3) ]] && echo -e "\r$green[+]$grey Service credentials Successfully created" || error "Échec lors de la création du service d'identifiants [4/4]" "Failed when creating the identifier service [4/4]"

	echo -en "\rCreate the Cinder service API endpoints ..."
	[[ $(openstack endpoint create --region RegionOne volumev2 public http://controller:8776/v2/%\(project_id\)s) ]] && echo -en "\rCreate the Cinder service API endpoints [1/6]" || error "Échec lors de la création du point de sortie du service API Cinder [1/6]" "Failed when creating the endpoint of the Cinder API Service [1/6]"
	[[ $(openstack endpoint create --region RegionOne volumev2 internal http://controller:8776/v2/%\(project_id\)s) ]] && echo -en "\rCreate the Cinder service API endpoints [2/6]" || error "Échec lors de la création du point de sortie du service API Cinder [2/6]" "Failed when creating the endpoint of the Cinder API Service [2/6]"
	[[ $(openstack endpoint create --region RegionOne volumev2 admin http://controller:8776/v2/%\(project_id\)s) ]] && echo -en "\rCreate the Cinder service API endpoints [3/6]" || error "Échec lors de la création du point de sortie du service API Cinder [3/6]" "Failed when creating the endpoint of the Cinder API Service [3/6]"
	[[ $(openstack endpoint create --region RegionOne volumev3 public http://controller:8776/v3/%\(project_id\)s) ]] && echo -en "\rCreate the Cinder service API endpoints [4/6]" || error "Échec lors de la création du point de sortie du service API Cinder [4/6]" "Failed when creating the endpoint of the Cinder API Service [4/6]"
	[[ $(openstack endpoint create --region RegionOne volumev3 internal http://controller:8776/v3/%\(project_id\)s) ]] && echo -en "\rCreate the Cinder service API endpoints [5/6]" || error "Échec lors de la création du point de sortie du service API Cinder [5/6]" "Failed when creating the endpoint of the Cinder API Service [5/6]"
	[[ $(openstack endpoint create --region RegionOne volumev3 admin http://controller:8776/v3/%\(project_id\)s) ]] && echo -e "\r$green[+]$grey Cinder service API endpoints Successfully created" || error "Échec lors de la création du point de sortie du service API Cinder [6/6]" "Failed when creating the endpoint of the Cinder API Service [6/6]"

	apt_package cinder-api cinder-scheduler lvm2 thin-provisioning-tools cinder-volume cinder-backup

	if [ ! -f /etc/cinder/cinder.conf ] ; then
		error "Le fichier de configuration de cinder n'existe pas" "Cinder configuartion file does not exist"
		exit $EXIT_FAILURE
	fi

	mv /etc/cinder/cinder.conf /etc/cinder/cinder.conf.$(date '+%Y-%m-%d')
	echo -e "$green[+]$grey Rename original configuration file to 'cinder.conf.$(date '+%Y-%m-%d')'"

	cp $CURRENT_PATH/etc/cinder.conf /etc/cinder/

	if [ ! -f /etc/cinder/cinder.conf ] ; then
		error "La copie du fichier de configuration a échoué" "Copying configuration file failed"
		exit $EXIT_FAILURE
	else
		echo -e "$green[+]$grey Copying configuration file Success"
	fi

	if [ -z $controller_ip ] ; then
		error "La variable 'CONTROLLER_IP' n'est pas définie" "The variable 'CONTROLLER_IP' is not defined"
		exit $EXIT_FAILURE
	fi

	if [ -z $cinder_dbpass ] ; then
		error "La variable 'CINDER_DBPASS' n'est pas définie" "The variable 'CINDER_DBPASS' is not defined"
		exit $EXIT_FAILURE
	fi

	if [ -z $rabbit_pass ] ; then
		error "La variable 'RABBIT_PASS' n'est pas définie" "The variable 'RABBIT_PASS' is not defined"
		exit $EXIT_FAILURE
	fi

	sed -ir "s/^[#]*\s*my_ip = CONTROLLER_IP.*/my_ip = ${controller_ip}/" /etc/cinder/cinder.conf
	sed -ir "s/^[#]*\s*transport_url = rabbit:\/\/openstack:.*/transport_url = rabbit:\/\/openstack:${rabbit_pass}@controller/" /etc/cinder/cinder.conf
	sed -ir "s/^[#]*\s*connection = mysql+pymysql.*/connection = mysql+pymysql:\/\/cinder:${cinder_dbpass}@controller\/cinder/" /etc/cinder/cinder.conf
	sed -ir "s/^[#]*\s*password = CINDER_PASS.*/password = ${cinder_pass}/" /etc/cinder/cinder.conf

	echo -e "$green[+]$grey Setting 'cinder.conf' Success"

	if [ -f /etc/cinder/cinder.confr ] ; then
		rm -f /etc/cinder/cinder.confr
	fi

	echo -en "\rPopulate the 'cinder' database ..."
	su -s /bin/sh -c "cinder-manage db sync" cinder 1> /dev/null 2>&1
	check_populate cinder

	service_restart nova-api cinder-scheduler apache2

	if ! ls /dev/sdb 1> /dev/null 2>&1 ; then
		error "Aucune partition SDB détectée" "No SDB partition detected"
		exit $EXIT_FAILURE
	else
		pvcreate /dev/sdb 1> /dev/null 2>&1
		echo -e "$green[+]$grey Create the LVM physical volume /dev/sdb"
		vgcreate cinder-volumes /dev/sdb 1> /dev/null 2>&1
		echo -e "$green[+]$grey Create the LVM volume group cinder-volumes"
	fi

	echo -e "$white[i]$grey Cinder backup service not installed because need Swift"
	
	service_restart tgt cinder-volume

	echo -en "\rCheck each volume enabled ..."
	sleep 5
	for i in $(openstack volume service list -c "Binary" -c "Status" -f csv | grep -v "Binary" | sed 's/ /_/g') ; do
		volume_name=$(echo -e "$i" | cut -d '"' -f 2 | sed 's/_/ /g')
		if [ $(echo -e "$i" | cut -d '"' -f 4) != "enabled" ] ; then
			error "Le volume '${volume_name}' n'est pas activé" "'${volume_name}' volume is not enabled"
			exit $EXIT_FAILURE
		fi
	done
	echo -e "\r$green[+]$grey All volumes are started   "

	# Installing the following service
	install_kuryr
}
