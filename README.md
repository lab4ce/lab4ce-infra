# Lab4CE Installer

[![Version](https://img.shields.io/badge/Version-0.1-blue.svg?style=for-the-badge)]()
[![Language](https://img.shields.io/badge/Bash-5.0%2B-brightgreen.svg?style=for-the-badge)]()
[![Available](https://img.shields.io/badge/OS-Ubuntu%2020.04-orange.svg?style=for-the-badge)]()
[![License](https://img.shields.io/badge/License-CeCILL--B-red.svg?style=for-the-badge)](http://cecill.info/licences/Licence_CeCILL-B_V1-en.html)
[![OpenStack](https://img.shields.io/badge/OpenStack-Victoria%2022th-blueviolet.svg?style=for-the-badge)]()

## A specific Lab4CE OpenStack installation

![Banner](https://user-images.githubusercontent.com/55319869/122503318-82e7cb00-cff8-11eb-9f51-00e4ef28c8d6.png)

Average duration of the installation: ~**30min**

## Prerequisites
### Harware configuration
(Mimimum)
- 4 Go RAM
- 10 Go Storage
- 1 CPU core

(Recommanded)
- 8+ Go RAM
- 100+ Go Storage
- 2-4+ CPUs

### In Addition
- 1 NIC, Internal network (In addition to the default one)
- Add a disk volume to ```/dev/sdb``` for the Cinder OpenStack service
- Ubuntu 20.04 LTS (Focal Fossa) fully compatible

## OpenStack Services
- Keystone : Identity service
- Glance : Image service
- Placement : Placement service
- Nova : Compute Service
- Neutron : Networking
- Horizon : Dashboard
- Cinder : Block Storage
- Kuryr : OpenStack Networking integration for containers
- Zun : Containers Service

## Help command result
```bash
$> ./install-lab4ce.sh -h
Usage: install-lab4ce.sh [[--install] [--restore CHECKPOINT]] [--cpu]

  -h, --help	 Print this message
  -i, --install	 Install OpenStack for Lab4CE
  -r, --restore	 Restore from a checkpoint
  -v, --version	 Show current script version
  -c, --cpu	 Display the number of system CPU core available

  e.g.  ./install-lab4ce.sh --install --restore [CHECKPOINT]
```

## Download & Install
Necessary to have root rights
```bash
$> git clone https://bitbucket.org/lab4ce/lab4ce-infra.git
$> cd lab4ce-infra/
$> ./install-lab4ce.sh --install
```

## Resume the course of the installation 
e.g. Resume installation at the **keystone** thanks to file ```install.restore```
```bash
$> ./install-lab4ce.sh --install
[?] Resume installation (Y/n)
```

Or manually
```bash
$> ./install-lab4ce.sh --install --restore keystone
```

## Other tools
- **check_script** : Check the contents of an OpenStack configuration file
- **check_logs** : Check OpenStack log files for debugging
- **check_services** : Check that all OpenStack services are started


## Reminder creation of a Container
```bash
$> openstack appcontainer run --name ct1 --net network={KURYR_NETWORK_ID} cirros ping 8.8.8.8
$> openstack appcontainer list
$> openstack appcontainer exec --interactive ct1 /bin/sh
$> openstack appcontainer stop ct1
$> openstack appcontainer delete ct1 --force
```

## Tool Overview
![Preview_1](https://user-images.githubusercontent.com/55319869/124105509-8ef57300-da63-11eb-99bc-9ef3262e74d1.png)
![Preview_2](https://user-images.githubusercontent.com/55319869/122503553-f25dba80-cff8-11eb-9607-261c06d860d8.png)
![Preview_3](https://user-images.githubusercontent.com/55319869/122503581-fee21300-cff8-11eb-8337-1d770a8fef29.png)

## License
CeCILL-B for Lab4CE Installer
